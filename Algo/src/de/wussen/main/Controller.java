package de.wussen.main;

import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.image.BufferedImage;
import java.awt.image.PixelGrabber;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import de.wussen.gui.InputPanel;
import de.wussen.gui.ProgressDialog;
import de.wussen.gui.View;
import de.wussen.gui.imagePanel.CenterPanel;
import de.wussen.gui.imagePanel.ToolPanel;
import de.wussen.listener.DrawingMouseListener;
import de.wussen.listener.InputListener;
import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.utils.AlgoUtils;
import de.wussen.utils.Log;

public class Controller extends Thread {

	private Model model;
	private View view;
	private File[] files;
	private Fading threadFading;
	private DrawingMouseListener drawingMouseListener;
	private InputListener inputListener;
	private String selectedToolBuffer;
	private Picture pictureBuffer;
	private boolean northBlocked = false;
	private boolean westBlocked = false;

	public Controller(boolean dev, boolean full) {
		Log.info("Creating controller <" + this.getClass() + ">");
//		model = new Model(5, 5, dev, full);
		model = new Model(500, 400, dev, full);
//		model = new Model(1024, 574, dev, full);
		view = new View(model);
		init();
		view.setVisible(true);
		view.pack();
		view.setLocationRelativeTo(null);
		if(model.isFull ()) {
			Log.info("fullscreen activated <" + this.getClass() + ">");
			view.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		}
		if(model.isDev()) {
			Log.info("Preloading pictures (DEV Mode) <" + this.getClass() + ">");
			loadWithPixelGrabber();
		}
	}

	private void init() {
		listener();
		menuBar();
		createWhitePicture();
		view.getCenterPanel().revalidate();
	}

	private void listener() {

		Log.info("Init listener <" + this.getClass() + ">");
		drawingMouseListener = new DrawingMouseListener(model, view);
		inputListener = new InputListener(model, view);

		view.getCenterPanel().addMouseListener(drawingMouseListener);
		view.getCenterPanel().addMouseMotionListener(drawingMouseListener);
		view.addKeyListener(inputListener);

		for (ToolPanel tp : model.getTools()) {
			tp.addMouseListener(new MouseAdapter() {

				@Override
				public void mousePressed(MouseEvent e) {
					if(!westBlocked) {
						if (model.getSelectedTool().equals("MOUSE") && !tp.getName().equals("MOUSE")) {
							System.out.println("MOUSE DESELECTED");
							view.getCenterPanel().saveChangesMadeOnFirstLayer();
							disableAllToolsFromNorth();
						} else if (!model.getSelectedTool().equals("MOUSE") && tp.getName().equals("MOUSE")) {
							enableAllToolsFromNorth();
						}
						unselectAllTools();
						tp.setSelectedPicture();
						model.setSelectedTool(tp.getName());
						view.getContentPane().repaint();
					}
				}

			});
		}

		view.getCenterPanel().addMouseWheelListener(new MouseWheelListener() {

			@Override
			public void mouseWheelMoved(MouseWheelEvent e) {
				if (model.isCtrlDown()) {
					if (e.getWheelRotation() == 1) {
						view.getCenterPanel().scale(0.8D, 0.8D, drawingMouseListener.getCurrentXPos(),
								drawingMouseListener.getCurrentYPos());
					} else {
						view.getCenterPanel().scale(1.25D, 1.25D, drawingMouseListener.getCurrentXPos(),
								drawingMouseListener.getCurrentYPos());
					}
				}
			}
		});
		view.setFocusable(true);

		// NorthPanel Buttons
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsMorph()) {
			tp.addMouseListener(new MouseAdapter() {

				@Override
				public void mousePressed(MouseEvent e) {
					if (!tp.isBlocked()) {
						tp.setSelectedPicture();
						morphingFromButton(tp.getName());
					}
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					tp.setSelectedPicture();
				}
			});
		}

		// NorthPanel Save
		view.getNorthPanelMoreTools().getTpSave().addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				view.getNorthPanelMoreTools().getTpSave().setSelectedPicture();
				if (!view.getNorthPanelMoreTools().getTpSave().isBlocked()) {
					savingImageToSelectionPanel();
					view.getContentPane().repaint();
				}
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				view.getNorthPanelMoreTools().getTpSave().setSelectedPicture();
				view.getNorthPanelMoreTools().getTpSave().setBlocked(true);
			}
		});

		// NorthPanel CopyPasteCutDelete
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsCopyAndPaste()) {
			tp.addMouseListener(new MouseAdapter() {

				@Override
				public void mousePressed(MouseEvent e) {
					if (!tp.isBlocked()) {
						tp.setSelectedPicture();
						copyAndPaste(tp.getName());
						view.getSouthPanel().repaint();
					}
				}

				@Override
				public void mouseReleased(MouseEvent e) {
					tp.setSelectedPicture();
				}
			});
		}

		view.getNorthPanelMoreTools().getTpAdd().addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				view.getNorthPanelMoreTools().getTpAdd().setSelectedPicture();
				selectFiles();
				view.getNorthPanelMoreTools().getTpAdd().setSelectedPicture();
				view.getContentPane().repaint();
				view.getContentPane().revalidate();
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				view.getNorthPanelMoreTools().getTpAdd().setSelectedPicture();
			}
		});

	}

	private void enableAllToolsFromNorth() {
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsMorph()) {
			tp.setBlocked(false);
		}
	}

	private void disableAllToolsFromNorth() {
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsMorph()) {
			tp.setBlocked(true);
		}
	}
	
	private void enableAllNorthAndWest() {
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsMorph()) {
			tp.setBlocked(false);
		}
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsCopyAndPaste()) {
			tp.setBlocked(false);
		}
		northBlocked = false;
		westBlocked = false;
	}
	
	private void disableAllNorthAndWest() {
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsMorph()) {
			tp.setBlocked(true);
		}
		for (ToolPanel tp : view.getNorthPanelMoreTools().getToolsCopyAndPaste()) {
			tp.setBlocked(true);
		}
		northBlocked = true;
		westBlocked = true;
	}

	private void unselectAllTools() {
		for (ToolPanel tp : model.getTools()) {
			tp.unselect();
		}
	}

	private void menuBar() {
		Log.info("Creating JMenus and JMenuItems <" + this.getClass() + ">");
		
		JMenu mFile = new JMenu("File");
		JMenuItem miFileChooser = new JMenuItem("Load Images");
		miFileChooser.addActionListener(e -> selectFiles());
		mFile.add(miFileChooser);

		JMenu mFading = new JMenu("Fading");
		JMenuItem miStartStop = new JMenuItem("Start");
		miStartStop.addActionListener(e -> fading(miStartStop));
		mFading.add(miStartStop);

		JMenu mPicture = new JMenu("Picture");
		JMenuItem miHistogram = new JMenuItem("Histogram");
		JMenuItem miApproximation = new JMenuItem("Approximation");
		JMenuItem miRemoveRed = new JMenuItem("Remove Red");
		JMenuItem miRemoveGreen = new JMenuItem("Remove Green");
		JMenuItem miRemoveBlue = new JMenuItem("Remove Blue");
		miHistogram.addActionListener(e -> showHistogram());
		miApproximation.addActionListener(e -> doApproximation());
		miRemoveRed.addActionListener(e -> view.getCenterPanel().removeOneColor(0xFF00FFFF));
		miRemoveGreen.addActionListener(e -> view.getCenterPanel().removeOneColor(0xFFFF00FF));
		miRemoveBlue.addActionListener(e -> view.getCenterPanel().removeOneColor(0xFFFFFF00));
		mPicture.add(miHistogram);
		mPicture.add(miApproximation);
		mPicture.add(miRemoveRed);
		mPicture.add(miRemoveGreen);
		mPicture.add(miRemoveBlue);

		JMenu mMorph = new JMenu("Morphing");
		JMenuItem miRotate = new JMenuItem("Rotate");
		JMenuItem miXShearing = new JMenuItem("X Shearing");
		JMenuItem miYShearing = new JMenuItem("Y Shearing");
		JMenuItem miTranslation = new JMenuItem("Translation");
		JMenuItem miScale = new JMenuItem("Scale");
		miRotate.addActionListener(e -> morphing("ROTATE"));
		miXShearing.addActionListener(e -> morphing("XSHEARING"));
		miYShearing.addActionListener(e -> morphing("YSHEARING"));
		miTranslation.addActionListener(e -> morphing("TRANSLATION"));
		miScale.addActionListener(e -> morphing("SCALE"));
		mMorph.add(miRotate);
		mMorph.add(miXShearing);
		mMorph.add(miYShearing);
		mMorph.add(miTranslation);
		mMorph.add(miScale);

		view.getViewMenuBar().add(mFile);
		view.getViewMenuBar().add(mFading);
		view.getViewMenuBar().add(mPicture);
		view.getViewMenuBar().add(mMorph);
	}

	private void doApproximation() {
//		String s = JOptionPane.showInputDialog(view, "Please enter a shearing factor");
//		try {
//			int i = Integer.parseInt(s);
//			Log.info("Approximation with: " + i + "% <" + this.getClass() + ">");
//			view.getCenterPanel().approximate(i);
//		} catch (Exception e) {
//			JOptionPane.showMessageDialog(view, "The input was not a number");
//		}
		if(!northBlocked) {
			view.getCenterPanel().approximatePreWork();
		}
	}

	private void copyAndPaste(String type) {
		Log.info("Selected copyAndPaste: " + type + " <" + this.getClass() + ">");
		System.out.println(type + " Pressed");
		if ("Copy".equals(type)) {
			Point pressed1 = model.getMousePressed1();
			Point pressed2 = model.getMousePressed2();
			int[] cache = view.getCenterPanel().copySelectionOrSecondLayer(false);
			if (cache != null) {
				model.setCache(cache, pressed1, pressed2);
			}
		}
		if ("Cut".equals(type)) {
			Point pressed1 = model.getMousePressed1();
			Point pressed2 = model.getMousePressed2();
			int[] cache = view.getCenterPanel().copySelectionOrSecondLayer(true);
			if (cache != null) {
				model.setCache(cache, pressed1, pressed2);
			}
		}
		if ("Paste".equals(type)) {
			if (model.getCache() != null) {
				view.getCenterPanel().pasteFromCache(model.getCache());
			}
		}
		if ("Delete".equals(type)) {
			view.getCenterPanel().deleteSelectionOnFirstLayer(true);
		}
	}

	private void morphing(String type) {
		if(!northBlocked) {
			System.out.println(type + " selected");
			Log.info("--- Selected mophing from JMenu: " + type + " <" + this.getClass() + ">");
			if ("ROTATE".equals(type)) {
				rotate();
			}
			if ("XSHEARING".equals(type)) {
				xShearing();
			}
			if ("YSHEARING".equals(type)) {
				yShearing();
			}
			if ("TRANSLATION".equals(type)) {
				translation();
			}
			if ("SCALE".equals(type)) {
				scale();
			}
			Log.info("--- End of morphing: " + type + " <" + this.getClass() + ">");
		}
	}

	private void morphingFromButton(String type) {
		Log.info("--- Selected mophing from ToolPanelNorth: " + type + " <" + this.getClass() + ">");
		if ("Left".equals(type)) {
			view.getCenterPanel().translation(-50, 0);
		}
		if ("Right".equals(type)) {
			view.getCenterPanel().translation(50, 0);
		}
		if ("Up".equals(type)) {
			view.getCenterPanel().translation(0, -50);
		}
		if ("Down".equals(type)) {
			view.getCenterPanel().translation(0, 50);
		}
		if ("ScalePlus".equals(type)) {
			view.getCenterPanel().scale(1.25D, 1.25D);
		}
		if ("ScaleMinus".equals(type)) {
			view.getCenterPanel().scale(0.8D, 0.8D);
		}
		if ("RotateLeft".equals(type)) {
			view.getCenterPanel().rotate(-45);
		}
		if ("RotateRight".equals(type)) {
			view.getCenterPanel().rotate(45);
		}
		if ("XShearingPlus".equals(type)) {
			view.getCenterPanel().xShearing(0.1D);
		}
		if ("XShearingMinus".equals(type)) {
			view.getCenterPanel().xShearing(-0.1D);
		}
		if ("YShearingPlus".equals(type)) {
			view.getCenterPanel().yShearing(0.1D);
		}
		if ("YShearingMinus".equals(type)) {
			view.getCenterPanel().yShearing(-0.1D);
		}
		Log.info("--- End of morphing: " + type + " <" + this.getClass() + ">");
	}

	private void xShearing() {
		String s = JOptionPane.showInputDialog(view, "Please enter a shearing factor");
		try {
			double d = Double.parseDouble(s);
			view.getCenterPanel().xShearing(d);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(view, "The input was not a number");
		}
	}

	private void yShearing() {
		String s = JOptionPane.showInputDialog(view, "Please enter a shearing factor");
		try {
			double d = Double.parseDouble(s);
			view.getCenterPanel().yShearing(d);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(view, "The input was not a number");
		}
	}

	private void translation() {
		InputPanel inputPanel = new InputPanel(view);
		int ok = JOptionPane.showConfirmDialog(view, inputPanel, "Please enter a X and Y Value for translation",
				JOptionPane.OK_CANCEL_OPTION);
		if (ok == JOptionPane.OK_OPTION) {
			double x = inputPanel.getXValue();
			double y = inputPanel.getYValue();
			if (x != ~0 && y != ~0) {
				view.getCenterPanel().translation(x, y);
			}
		}
	}

	private void scale() {
		InputPanel inputPanel = new InputPanel(view, 0.85D, 0.85D);
		int ok = JOptionPane.showConfirmDialog(view, inputPanel, "Please enter a X and Y Value for scaling",
				JOptionPane.OK_CANCEL_OPTION);
		if (ok == JOptionPane.OK_OPTION) {
			double x = inputPanel.getXValue();
			double y = inputPanel.getYValue();
			if (x != ~0 && y != ~0) {
				view.getCenterPanel().scale(x, y);
			}
		}
	}

	private void rotate() {
		String s = JOptionPane.showInputDialog(view, "Please enter a degree number between '-359' - '359'");
		try {
			double d = Double.parseDouble(s);
			if (d > -360 && d < 360) {
				view.getCenterPanel().rotate(d);
			} else {
				JOptionPane.showMessageDialog(view, "The input was not between '-359' - '359'");
			}
		} catch (Exception e) {
			JOptionPane.showMessageDialog(view, "The input was not a number");
		}
	}

	private void savingImageToSelectionPanel() {
		view.getCenterPanel().saveToSelection();
	}

	private void showHistogram() {
		if(!northBlocked) {
			int rgb[] = view.getCenterPanel().getPicture().getArr();
			StringBuilder histogramAsText = new StringBuilder();
			HashMap<String, Integer> hmColors = new HashMap<>(model.getWidth() * model.getHeight());
			String fileName = view.getCenterPanel().getPicture().getFileName();
			
			System.out.println("Save Histogram from <" + fileName + ">");
			
			for (int i = 0; i < rgb.length; i++) {
				String hex = Integer.toHexString(rgb[i]);
				Integer colorCount = hmColors.get(hex);
				if (colorCount == null) {
					colorCount = 0;
				}
				
				++colorCount;
				hmColors.put(hex, colorCount);
			}
	
			for (Entry<String, Integer> map : hmColors.entrySet()) {
				if (map.getKey() != null) {
					histogramAsText.append(map.getKey() + ": " + map.getValue() + System.lineSeparator());
				}
			}
	
			AlgoUtils.printToLogFile(fileName, histogramAsText.toString());
		}
	}

	private void fading(JMenuItem miStartStop) {
		if (miStartStop.getText().equals("Start")) {
			for (Picture picture : model.getPictures()) {
				if (picture.isSelected()) {
					view.getCenterPanel().clearSecondLayer(model);
					model.setMousePressed1(null);
					model.setMousePressed2(null);
					model.addPictureToSelectPictureVector(picture);
				}
			}
			if (model.getSelectedPictures().size() >= 2) {
				pictureBuffer = view.getCenterPanel().getPicture();
				disableAllNorthAndWest();
				selectedToolBuffer = model.getSelectedTool();
				model.setSelectedTool("");
				miStartStop.setText("Stop");
				threadFading = new Fading(model, view.getCenterPanel());
				threadFading.start();
			}
		} else {
			miStartStop.setText("Start");
			model.setSelectedTool(selectedToolBuffer);
			selectedToolBuffer = "";
			threadFading.setRunning(false);
			threadFading = null;
			enableAllNorthAndWest();
			model.setSelectedPictures(new Vector<>());
			view.getCenterPanel().setPicture(pictureBuffer);
			pictureBuffer = null;;
		}
	}

	private void selectFiles() {
		try {
			JFileChooser fc = new JFileChooser(System.getProperty("user.home"));
			fc.setMultiSelectionEnabled(true);
			fc.addChoosableFileFilter(new FileNameExtensionFilter("Images", "jpg", "gif"));
			fc.setAcceptAllFileFilterUsed(false);
			fc.showOpenDialog(view);
			File[] files = fc.getSelectedFiles();
			this.files = files;
			Thread t = new Thread(this);
			t.start();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void loadWithPixelGrabber() {
		Log.info("Init program with pre selected Pictures <" + this.getClass() + ">");
		loadWithPixelGrabber(new File("0.jpg"));
		loadWithPixelGrabber(new File("4.jpg"));
		loadWithPixelGrabber(new File("5.jpg"));
		loadWithPixelGrabber(new File("6.jpg"));
		loadWithPixelGrabber(new File("7.jpg"));
		loadWithPixelGrabber(new File("8.jpg"));
		loadWithPixelGrabber(new File("9.jpg"));
		loadWithPixelGrabber(new File("10.jpg"));
		loadWithPixelGrabber(new File("11.jpg"));
		loadWithPixelGrabber(new File("12.jpg"));
		loadWithPixelGrabber(new File("13.jpg"));
		loadWithPixelGrabber(new File("14.jpg"));
		loadWithPixelGrabber(new File("15.jpg"));
		loadWithPixelGrabber(new File("27.gif"));
	}
	private void loadWithPixelGrabber(File file) {
		addNewImageToModel(file);
		view.getCenterPanel().revalidate();
	}

	private void createWhitePicture() {
		view.getSouthPanel().addImage(model.addPicture(view.createWhitePicture()));
	}

	private void addNewImageToModel(File file) {
		try {
			BufferedImage tmp = ImageIO.read(file);
			Image image = tmp.getScaledInstance(model.getWidth(), model.getHeight(), BufferedImage.SCALE_SMOOTH);

			int[] arr = new int[model.getWidth() * model.getHeight()];

			PixelGrabber pg = new PixelGrabber(image, 0, 0, model.getWidth(), model.getHeight(), arr, 0,
					model.getWidth());
			pg.grabPixels();
			view.getSouthPanel().addImage(model.addPicture(arr, image, file.getName())); 
		} catch (InterruptedException | IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		ProgressDialog progressDialog = new ProgressDialog(this.files.length);
		try {
			progressDialog.setAlwaysOnTop(true);
			view.setFocusable(false);
			for (File file : this.files) {

				BufferedImage tmp = ImageIO.read(file);
				Image image = tmp.getScaledInstance(model.getWidth(), model.getHeight(), BufferedImage.SCALE_SMOOTH);

				int[] arr = new int[model.getWidth() * model.getHeight()];

				PixelGrabber pg = new PixelGrabber(image, 0, 0, model.getWidth(), model.getHeight(), arr, 0,
						model.getWidth());
				pg.grabPixels();
				view.getSouthPanel().addImage(model.addPicture(arr, image, file.getName()));
				progressDialog.progress(file.getName());
				view.getContentPane().revalidate();
				view.getSouthPanel().repaint();
			}
			progressDialog.dispose();
			view.setFocusable(true);
		} catch (Exception e) {
			progressDialog.dispose();
			view.setFocusable(true);
		}
		this.files = null;
	}

}
