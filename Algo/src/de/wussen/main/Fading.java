package de.wussen.main;

import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.util.Vector;

import de.wussen.gui.imagePanel.CenterPanel;
import de.wussen.model.Model;
import de.wussen.model.Picture;

public class Fading extends Thread {
	
	private Model model;
	private CenterPanel centerPanel;
	private volatile boolean running = true;
	private Picture pictureBuffer;
	private Picture picturePointerBuffer;
	
	public Fading(Model model, CenterPanel centerPanel) { 
		this.model = model;
		this.centerPanel = centerPanel;
		this.pictureBuffer = centerPanel.getPicture();
		this.picturePointerBuffer = centerPanel.getPicturePointer();
	}
	
	@Override
	public void run() {
		Vector<Picture> pictures = model.getSelectedPictures();
		int arr1[] = new int[model.getWidth() * model.getHeight()];
		int arr2[] = new int[model.getWidth() * model.getHeight()];
		int arr3[] = new int[model.getWidth() * model.getHeight()];
		int index = 0;
		int loop = 128;
		centerPanel.setPicture(pictures.get(0));
		while (running) {			
			arr1 = pictures.get(index).getArr();
//			centerPanel.setSecondPicture(pictures.get(index));
			if(index >= pictures.size()-1) {
				arr2 = pictures.get(0).getArr().clone(); 
				index = 0;
			} else {
				arr2 = pictures.get(++index).getArr().clone();
			}
			
			waitXMs(4000);
			
			for (int i = 0; i < loop; i++) {
				
//				fade(arr,  2);
				for (int j = 0; j < arr1.length; j++) {					
					arr3[j] = colorFade(arr1[j], arr2[j], (int) (((float) i / (float)(loop-1)) * 100f));
				}
				
				MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), arr3, 0, model.getWidth());
				mis.setAnimated(true);
				centerPanel.setPictureFromMemoryImageSource(arr3, mis);
				centerPanel.repaint();
				
				waitXMs(5);
			}
		}
		centerPanel.setPicture(this.pictureBuffer);
		centerPanel.setPicturePointer(this.picturePointerBuffer);
	}
	
	private void waitXMs(int i) {
		try {
			Thread.sleep(i);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void fade(int arr[], int nr) {
		for (int i = 0; i < arr.length; i++) {
			int rgb = arr[i] << 8;
			rgb = rgb >>> 8;
			
			int a = arr[i] >>> 24;
			a += nr;
			a = a << 24;
			
			arr[i] = a | rgb;
		}
	}
	
	private int colorFade(int rgb1, int rgb2, int percent) {
		int red   = singleSchuffle((rgb1 >> 16) & 255, (rgb2 >> 16) & 255, percent);
		int green = singleSchuffle((rgb1 >> 8)  & 255, (rgb2 >> 8)  & 255, percent);
		int blue  = singleSchuffle((rgb1) 		& 255, (rgb2) 		& 255, percent);
		return 255 << 24 | red << 16 | green << 8 | blue;
	}

	private int singleSchuffle(int rgb1, int rgb2, int percent) {
		return rgb1 + (rgb2 - rgb1) * percent / 100;
	}

	public void setRunning(boolean run) {
		this.running = run;
	}

}
