package de.wussen.main;

import de.wussen.model.Model;
import de.wussen.utils.Log;

public class Main {

	private static boolean dev = false;
	
	public static void main(String[] args) {
		boolean full = false;
		
		for (int i = 0; i < args.length; i++) {
			switch (args[i]) {
			
				case "developer":
					dev = true;
					Log.setFileName(Model.TITLE);
					Log.setDev(dev);
					Log.checkDirectory();
					Log.info("Starting");
					break;
					
				case "full":
					full = true;
					break;
			}
		}
		
		Controller ctrl = new Controller(dev, full);

	}

}
