package de.wussen.main;

import de.wussen.model.Vector;

public class Matrix {
	
	private double[][] matrix;
	
	public Matrix(double[][] matrix) {
		this.matrix = matrix;
	}
	
	public static Matrix getStandartMatrix() {
		return new Matrix(new double[][] {	{1,0, 0}, 
											{0,1, 0},
											{0,0, 1}});
	}
	
	public static Matrix translation(double dx, double dy) {
		return new Matrix(new double[][] {	{1,0,dx}, 
											{0,1,dy},
											{0,0, 1}});
	}
	
	public static Matrix inverseTranslation(double dx, double dy) {
		return new Matrix(new double[][] {	{1,0,-dx}, 
											{0,1,-dy},
											{0,0,  1}});
	}
	
	public static Matrix rotation(double a) {
		return new Matrix(new double[][] {	{Math.cos(a), -Math.sin(a), 0}, 
											{Math.sin(a),  Math.cos(a), 0},
											{		   0,		     0, 1}});
	}
	
	public static Matrix inverseRotation(double a) {
		return new Matrix(new double[][] {	{ Math.cos(a),  Math.sin(a), 0}, 
											{-Math.sin(a),  Math.cos(a), 0},
											{		    0,		      0, 1}});
	}
	
	public static Matrix scale(double dx, double dy) {
		return new Matrix(new double[][] {	{dx,  0, 0}, 
											{ 0, dy, 0},
											{ 0,  0, 1}});
	}
	
	public static Matrix inverseScale(double dx, double dy) {
		return new Matrix(new double[][] {	{1/dx,    0, 0}, 
											{   0, 1/dy, 0},
											{   0,    0,  1}});
	}
	
	public static Matrix xShearing(double dx) {
		return new Matrix(new double[][] {	{ 1, dx, 0}, 
											{ 0,  1, 0},
											{ 0,  0, 1}});
	}
	
	public static Matrix inverseXShearing(double d) {
		return new Matrix(new double[][] {	{ 1, -d, 0}, 
											{ 0,   1, 0},
											{ 0,   0, 1}});
	}
	
	public static Matrix yShearing(double dy) {
		return new Matrix(new double[][] {	{ 1,  0, 0}, 
											{dy,  1, 0},
											{ 0,  0, 1}});
	}
	
	public static Matrix inverseYShearing(double dy) {
		return new Matrix(new double[][] {	{  1,   0, 0}, 
											{-dy,   1, 0},
											{  0,   0, 1}});
	}

	public double[][] getMatrix() {
		return matrix;
	}

	public static Vector mult(Vector vector, Matrix matrixObj) {
		double[][] matrix = matrixObj.getMatrix();
		Vector newVector = new Vector();
		newVector.setV1((int) (matrix[0][0] * vector.getV1() + matrix[0][1] * vector.getV2() + matrix[0][2] * vector.getV3()));
		newVector.setV2((int) (matrix[1][0] * vector.getV1() + matrix[1][1] * vector.getV2() + matrix[1][2] * vector.getV3()));
		return newVector;
	}
	
	public static Matrix mult(Matrix matrixObj1, Matrix matrixObj2) {
		double[][] newDouble = new double[3][3];
		double[][] oldDouble1 = matrixObj1.getMatrix();
		double[][] oldDouble2 = matrixObj2.getMatrix();
		
		for (int y = 0; y < 3; y++) {
			for (int x = 0; x < 3; x++) {
				for (int z = 0; z < 3; z++) {
					newDouble[y][x] += oldDouble1[y][z] * oldDouble2[z][x];
				}
			}
		}
		
		return new Matrix(newDouble);
	}
	
	public boolean isEqual(Matrix matrix) {
		double[][] matrixArr = matrix.getMatrix();
		for (int y = 0; y < this.matrix.length; y++) {
			for (int x = 0; x < this.matrix[y].length; x++) {
				if(this.matrix[y][x] != matrixArr[y][x]) {
					return false;
				}
			}
		}
		return true;
	}
	
}
