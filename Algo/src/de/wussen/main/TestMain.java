package de.wussen.main;

import de.wussen.utils.AlgoUtils;

public class TestMain {
	
	public static void main(String[] args) {
		
		int zuErsetzendeFarbe = Integer.parseInt("0403FE", 16);
		
		int arr[] = new int[10];
		arr[0] = Integer.parseInt("010106", 16);
		arr[1] = Integer.parseInt("010207", 16);
		arr[2] = Integer.parseInt("020803", 16);
		arr[3] = Integer.parseInt("030502", 16);
		arr[4] = Integer.parseInt("040107", 16);
		arr[5] = Integer.parseInt("040301", 16);
		arr[6] = Integer.parseInt("0403FF", 16);
		arr[7] = Integer.parseInt("040509", 16);
		arr[8] = Integer.parseInt("050802", 16);
		arr[9] = Integer.parseInt("050905", 16);
		
		for (int i = 0; i < arr.length; i++) { 
			int res = AlgoUtils.approximationCalculationTest(arr[i], zuErsetzendeFarbe);
			System.out.println(arr[i] + " Distance: " + res);
		}
	}
	
}
