package de.wussen.utils;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import de.wussen.gui.imagePanel.CenterPanel;
import de.wussen.model.ApproximationColor;
import de.wussen.model.Model;
import de.wussen.model.Vector;

public class AlgoUtils {
	
	/**
	 * calculates the top left corner and bottom right corner from two Points.
	 * Returns an array with new Coordinates
	 * [0] Top Left X
	 * [1] Top Left Y
	 * [2] Bottom Right X
	 * [3] Bottom Right Y
	 * 
	 * @param Point point1
	 * @param Point point2
	 * @return int[] 
	 */
	public static int[] calcMousePos(Point point1, Point point2, CenterPanel centerPanel, Model model) {
		int[] pos = new int[4];
		double xFactor = ((double) centerPanel.getWidth()) / ((double) model.getWidth());
		double yFactor = ((double) centerPanel.getHeight()) / ((double) model.getHeight());
		if(point1 != null && point2 != null) {
			if(point1.x <= point2.x) {
				pos[0] = (int) (point1.x / xFactor);
				pos[2] = (int) (point2.x / xFactor);
			} else {
				pos[0] = (int) (point2.x / xFactor);
				pos[2] = (int) (point1.x / xFactor);
			}
			
			if(point1.y <= point2.y) {
				pos[1] = (int) (point1.y / yFactor);
				pos[3] = (int) (point2.y / yFactor);
			} else {
				pos[1] = (int) (point2.y / yFactor);
				pos[3] = (int) (point1.y / yFactor);
			}
		} else {
			System.err.println("CalcMousePos, mousePressed 1 || 2 == null" );
		}
			
		return pos;
	}

//	public static Picture createBlankPicture(Model model) {
//		return new Picture(new int[model.getWidth() * model.getHeight()], img);
//	}
	
	/**
	 * 
	 */
	public static void printToLogFile(String fileName, String txt) {
		String file = "histogramme" + File.separatorChar + fileName + ".txt";
		System.out.println("Saving histogram to txt. Path: " + file);
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			out.write(txt);
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
//	/**
//	 * 
//	 * @param fileName
//	 * @return vector with all colors
//	 */
//	public static Vector<Integer> loadVectorFromHistogram(String fileName, Model model) {
//		try {
//		
//			String file = "histogramme" + File.separatorChar + fileName + ".txt";
//			FileReader fr = new FileReader(file);
//			BufferedReader br = new BufferedReader(fr);
//			int size = model.getWidth() * model.getHeight();
//			Vector<Integer> vecHisto = new Vector<>(size);
//			HashMap<String, Integer> hmHisto = new HashMap<>(size);
//
//			for (String line = br.readLine(); line != null; line = br.readLine()) {
//				hmHisto.put(line.substring(0, 7), Integer.parseInt(line.substring(10, 11)));
//			}
//			
//			
//			
//		} catch (IOException e) {
//			Log.error(e, AlgoUtils.class.getName());
//		}
//
//		return null;
//	}
	
    public static void quicksortTwoArrays(int[] hexKeys, int[] hexQuant) {
    	quicksortHelp(hexKeys, hexQuant, 0, hexQuant.length - 1);
	}

	private static void quicksortHelp(int[] hexKeys, int[] hexQuant, int iLeft, int iRight) {
    	final int MID = hexQuant[(iLeft + iRight) / 2];
    	int l = iLeft;
    	int r = iRight;
    	
    	while(l < r) {
    		while(hexQuant[l] < MID) {
    			++l;
    		}
    		while(hexQuant[r] > MID) {
    			--r;
    		}
    		if(l <= r) {
    			swap(hexKeys, hexQuant, l++, r--);
    		}
    	}
    	if(iLeft < r) {
    		quicksortHelp(hexKeys, hexQuant, iLeft, r);
    	}
    	if(iRight > l) {
    		quicksortHelp(hexKeys, hexQuant, l, iRight);
    	}
    }

	private static void swap(int[] hexKeys, int[] hexQuant, int i1, int i2) {
		int tmpKeys = hexKeys[i1];
		hexKeys[i1] = hexKeys[i2];
		hexKeys[i2] = tmpKeys;
		
		int tmpQuant = hexQuant[i1];
		hexQuant[i1] = hexQuant[i2];
		hexQuant[i2] = tmpQuant;
	}
	
	public static void quicksort(int[] arr, int shift) {
	    quicksortHelp(arr, 0, arr.length - 1, shift);
	}

	private static void quicksortHelp(int[] arr, int iLeft, int iRight, int shift) {
		final int MID = (arr[(iLeft + iRight) / 2] >> shift) & 255;
		int l = iLeft;
		int r = iRight;
		
		while(l < r) {
			while(((arr[l] >> shift) & 255) < MID) {
				++l;
			}
			while(((arr[r] >> shift) & 255) > MID) {
				--r;
			}
			if(l <= r) {
				swap(arr, l++, r--);
			}
		}
		if(iLeft < r) {
			quicksortHelp(arr, iLeft, r, shift);
		}
		if(iRight > l) {
			quicksortHelp(arr, l, iRight, shift);
		}
	}
	
	private static void swap(int[] arr, int i1, int i2) {
		int tmp = arr[i1];
		arr[i1] = arr[i2];
		arr[i2] = tmp;
	}
	
	public static Vector binarySearchVector(Vector[] arrR, int key) {
		int l = 0;
		int r = arrR.length-1;
		int mid = 0;
		while(l <= r) {
			mid = (l + r) / 2;
			final int v = arrR[mid].getV1();
			if(v == key) {
				return arrR[mid];
			} else if( v < key) {
				l = mid + 1;
			} else {
				r = mid - 1;
			}
		}
		return arrR[mid];
	}
	
	public static int[] binarySearch(int[] arr, int shift, int key) {
		int l = 0;
		int r = arr.length-1;
		int mid = 0;
		while(l <= r) {
			mid = (l + r) / 2;
			final int v = ((arr[mid] >> shift) & 255);
			if(v == key) {
				return new int[] {arr[mid],arr[mid], mid, mid};
			} else if( v < key) {
				l = mid + 1;
			} else {
				r = mid - 1;
			}
		}
		if(key > arr[mid]) {
			if(arr.length > mid+1) {
				return new int[] {arr[mid],arr[mid+1], mid, mid+1};
			} else {
				return new int[] {arr[mid],arr[mid], mid, mid};
			}
		} else {
			if(0 <= mid-1) {
				return new int[] {arr[mid-1],arr[mid], mid-1, mid};
			} else {
				return new int[] {arr[mid],arr[mid], mid, mid};
			}
		}
	}
	
	public static int binarySearchNew(int[] arr, int shift, int key) {
		int l = 0;
		int r = arr.length-1;
		int mid = 0;
		while(l <= r) {
			mid = (l + r) / 2;
			final int v = ((arr[mid] >> shift) & 255);
			if(v == key) {
				return mid;
			} else if( v < key) {
				l = mid + 1;
			} else {
				r = mid - 1;
			}
		}
		if(key > ((arr[mid] >> shift) & 255)) {
			return mid;
		} else {
			if(0 <= mid-1) {
				return mid-1;
			} else {
				return mid;
			}
		}
	}
	
	

	public static void getApproximationDistance(int col, int key, ApproximationColor ac) {
		int rRes = (int) Math.pow(((key >> 16) 	& 255) - ((col >> 16) 	& 255), 2);
		int gRes = (int) Math.pow(((key >>  8) 	& 255) - ((col >>  8) 	& 255), 2);
		int bRes = (int) Math.pow(((key) 		& 255) - ((col) 		& 255), 2);
		int rgbRes  = (int) Math.sqrt(rRes + gRes + bRes);
		if(rgbRes < ac.getMinDistance()) {
			ac.setMinDistance(rgbRes);
			ac.setNewColor(col);
//			System.out.println(Integer.toHexString(key) + " " + Integer.toHexString(col));
		}
	}
	
	public static int approximationCalculationTest(int col, int key) {
		int rRes = (int) Math.pow(((key >> 16) 	& 255) - ((col >> 16) 	& 255), 2);
		int gRes = (int) Math.pow(((key >>  8) 	& 255) - ((col >>  8) 	& 255), 2);
		int bRes = (int) Math.pow(((key) 		& 255) - ((col) 		& 255), 2);
		int rgbRes  = (int) Math.sqrt(rRes + gRes + bRes);
		return rgbRes;
	}

	public static void getclosestColor(int[] arrR, int[] arrG, int[] arrB, int indexR, int indexG, int indexB, int key, ApproximationColor ac) {
		int offs = 0;
		boolean search = true;
		while(search) {
			search = false;
			int ind;
			
			ind = indexR-offs;
			if(ind >= 0 && ((key >> 16) & 255) - ((arrR[ind] >> 16) & 255) <= ac.getMinDistance()) {
				getApproximationDistance(arrR[ind], key, ac);
				search = true;
			}
			
			ind = indexR+offs+1;
			if(ind < arrR.length && (((arrR[ind] >> 16) & 255) - (key >> 16) & 255) <= ac.getMinDistance()) {
				getApproximationDistance(arrR[ind], key, ac);
				search = true;
			}
			
			ind = indexG-offs;
			if(ind >= 0 && ((key >> 8) & 255) - ((arrR[ind] >> 8) & 255) <= ac.getMinDistance()) {
				getApproximationDistance(arrR[ind], key, ac);
				search = true;
			}
			
			ind = indexG+offs+1;
			if(ind < arrR.length && (((arrR[ind] >> 8) & 255) - (key >> 8) & 255) <= ac.getMinDistance()) {
				getApproximationDistance(arrR[ind], key, ac);
				search = true;
			}
			
			ind = indexB-offs;
			if(ind >= 0 && ((key) & 255) - ((arrR[ind]) & 255) <= ac.getMinDistance()) {
				getApproximationDistance(arrR[ind], key, ac);
				search = true;
			}
			
			ind = indexB+offs+1;
			if(ind < arrR.length && (((arrR[ind]) & 255) - (key) & 255) <= ac.getMinDistance()) {
				getApproximationDistance(arrR[ind], key, ac);
				search = true;
			}
			++offs;
		}
	}
	
	public static String getARGBString(int i) {
		int a = (i >> 24) & 255;
		int r = (i >> 16) & 255;
		int g = (i >>  8) & 255;
		int b = (i)       & 255;
		return String.valueOf(a) + String.valueOf(r) + String.valueOf(g) + String.valueOf(b) 
		+ " (" + (Integer.toHexString(a) + Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b)) + ")";
	}
	
	public static String getRGBString(int i) {
		int r = (i >> 16) & 255;
		int g = (i >>  8) & 255;
		int b = (i)       & 255;
		return String.valueOf(r) + String.valueOf(g) + String.valueOf(b) 
			+ " (" + (Integer.toHexString(r) + Integer.toHexString(g) + Integer.toHexString(b)) + ")";
	}
	
}
