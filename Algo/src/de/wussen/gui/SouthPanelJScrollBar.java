package de.wussen.gui;

import javax.swing.JScrollPane;

import de.wussen.model.Model;
import de.wussen.utils.Log;

public class SouthPanelJScrollBar extends JScrollPane {

	private static final long serialVersionUID = 1L;

	public SouthPanelJScrollBar(Model model, SouthPanelImageSelection soutPanel) {
		super(soutPanel, JScrollPane.VERTICAL_SCROLLBAR_NEVER, JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS);
		Log.info("Creating JScrollBar for south panel <" + this.getClass() + ">");
		setAutoscrolls(true);
	}
	
}
