package de.wussen.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;

import de.wussen.gui.imagePanel.CenterPanel;

public class ApproximationSliderPanel extends JDialog {
	
	private static final long serialVersionUID = 1L;
	private JSlider slider;
	private JPanel pPercent;
	private JTextField txtPercent;
	private JButton btnPercent;
	private JButton btnLeft;
	private JButton btnRight;
	private JButton btnSave; //TODO:
	private JButton btnCancel; //TODO:
	
	private boolean activated = true;
	
	//TODO: Textfield for colors instead of percent

	private int colLength;
	
	public ApproximationSliderPanel(CenterPanel centerPanel, int colLength) {
		this.colLength = colLength;
		
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.PAGE_AXIS));
		setBackground(Color.DARK_GRAY);

		slider = new JSlider(1, 100, 100);
		slider.setValue(100);
		slider.setMinorTickSpacing(5);
        slider.setMajorTickSpacing(10);
        slider.setExtent(5);
        slider.setPaintTicks(true);
		slider.addChangeListener(e -> changeListener(centerPanel));
		
		pPercent = new JPanel();
		pPercent.setLayout(new FlowLayout(FlowLayout.CENTER));
		txtPercent = new JTextField();
		txtPercent.setText(String.valueOf(100));
		txtPercent.setPreferredSize(new Dimension(100, 25));
		btnPercent = new JButton("Fire");
		btnPercent.addActionListener(e -> btnPercentListener(centerPanel));
		btnLeft = new JButton("<-5%");
		btnLeft.addActionListener(e -> btnFivePercentListener(-5, centerPanel));
		btnRight = new JButton("5%->");
		btnRight.addActionListener(e -> btnFivePercentListener(5, centerPanel));
		btnRight.setEnabled(false);
		pPercent.add(txtPercent);
		pPercent.add(btnPercent);
		pPercent.add(btnLeft);
		pPercent.add(btnRight);
		
		setTitle("Colors left / Colors max:  " + String.valueOf((int) colLength) + " / " + String.valueOf((int) colLength));
		
		add(slider);
		add(pPercent);
		
		setVisible(true);
		setLocationRelativeTo(centerPanel);
		setSize(400, 100);
	}

	private void btnFivePercentListener(int adjustment, CenterPanel centerPanel) {
		int newPercent = slider.getValue() + adjustment;
		if(newPercent < 1) {
			newPercent = 1;
		} else if(newPercent > 100) {
			newPercent = 100;
		}
		activated = false;
		slider.setValue(newPercent);
		activated = true;
		txtPercent.setText(String.valueOf(newPercent));
		double percent = newPercent/100.0D;
		setTitle("Colors left / Colors max:  " + String.valueOf((int) (colLength * percent)) + " / " + String.valueOf((int) colLength));
		disableBtn(percent);
		centerPanel.approximate(percent);
	}

	private void btnPercentListener(CenterPanel centerPanel) {
		double percent = Integer.parseInt(txtPercent.getText())/100.0D;
		setTitle("Colors left / Colors max:  " + String.valueOf((int) (colLength * percent)) + " / " + String.valueOf((int) colLength));
		activated = false;
		slider.setValue(Integer.parseInt(txtPercent.getText()));
		activated = true;
		disableBtn(percent);
		centerPanel.approximate(percent);
	}

	private void changeListener(CenterPanel centerPanel) {
        if(!slider.getValueIsAdjusting() && activated) {
        	 double percent = slider.getValue()/100.0D;
        	 setTitle("Colors left / Colors max:  " + String.valueOf((int) (colLength * percent)) + " / " + String.valueOf((int) colLength));
        	 txtPercent.setText(String.valueOf(slider.getValue()));
        	 disableBtn(percent);
        	 centerPanel.approximate(percent);
        }
	}
	
	private void disableBtn(double percent) {
		if(percent <= 0.01D) {
			btnLeft.setEnabled(false);
		} else {
			btnLeft.setEnabled(true);
		}
		if(percent >= 1.00D) {
			btnRight.setEnabled(false);
		} else {
			btnRight.setEnabled(true);
		}
	}

}
