package de.wussen.gui;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

import de.wussen.gui.imagePanel.ToolPanel;
import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.utils.Log;

public class WestPanelToolBox extends JPanel {
	
	private static final long serialVersionUID = 1L;

	private Model model;
	
	private Picture pictureMouse = null;
	private Picture pictureMouseSelected = null;
	private Picture pictureLine = null;
	private Picture pictureLineSelected = null;
	private Picture pictureLineColored = null;
	private Picture pictureLineColoredSelected = null;
	private Picture pictureCircleColored = null;
	private Picture pictureCircleColoredSelected = null;
	private Picture pictureCircle = null;
	private Picture pictureCircleSelected = null;
	private Picture pictureCircleFilledColored = null;
	private Picture pictureCircleFilledColoredSelected = null;
	private Picture pictureCircleFilled = null;
	private Picture pictureCircleFilledSelected = null;
	private Picture pictureColor = null;
	private Picture pictureColorSelected = null;
	private Picture picturePencil = null;
	private Picture picturePencilSelected = null;
	
	private ArrayList<ToolPanel> tools;
	
	public WestPanelToolBox(Model model) {
		Log.info("Creating west tools <" + this.getClass() + ">");
		this.model = model;
		BoxLayout bl = new BoxLayout(this, BoxLayout.PAGE_AXIS);
		setLayout(bl);
		setBackground(Color.DARK_GRAY);
		
		init();
		
		ArrayList<ToolPanel> tools = new ArrayList<>();
		
		tools.add(new ToolPanel(pictureMouse, pictureMouseSelected, "MOUSE"));
//		tools.add(new ToolPanel(picturePencil, picturePencilSelected, "PENCIL"));
		tools.add(new ToolPanel(pictureLine, pictureLineSelected, "LINE"));
		tools.add(new ToolPanel(pictureLineColored, pictureLineColoredSelected, "LINECOLORED"));
		tools.add(new ToolPanel(pictureCircle, pictureCircleSelected, "CIRCLE"));
		tools.add(new ToolPanel(pictureCircleFilled, pictureCircleFilledSelected, "CIRCLEFILLED"));
		tools.add(new ToolPanel(pictureCircleColored, pictureCircleColoredSelected, "CIRCLECOLORED"));
		tools.add(new ToolPanel(pictureCircleFilledColored, pictureCircleFilledColoredSelected, "CIRCLECOLOREDFILLED"));
//		tools.add(new ToolPanel(pictureColor, pictureColorSelected, "COLOR"));
		
		tools.get(0).setSelectedPicture();
		model.setSelectedTool("MOUSE");
		model.setTools(tools);
		
		for(ToolPanel tp : tools) {
			add(tp);
		}
			
	}

	private void init() {
		try {
			pictureMouse = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "Mouse.png")));
			pictureMouseSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "MouseSelected.png")));
			pictureLine = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "Line.png")));
			pictureLineSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "LineSelected.png")));
			pictureLineColored = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "LineColored.png")));
			pictureLineColoredSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "LineColoredSelected.png")));
			pictureCircle = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "Circle.png")));
			pictureCircleSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleSelected.png")));
			pictureCircleFilled = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleFilled.png")));
			pictureCircleFilledSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleFilledSelected.png")));
			pictureCircleColored = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleColored.png")));
			pictureCircleColoredSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleColoredSelected.png")));
			pictureCircleFilledColored = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleFilledColored.png")));
			pictureCircleFilledColoredSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "CircleFilledColoredSelected.png")));
			pictureColor = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "Color.png")));
			pictureColorSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "ColorSelected.png")));
			picturePencil = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "Pencil.png")));
			picturePencilSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + "PencilSelected.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
