package de.wussen.gui;

import java.awt.BorderLayout;
import java.awt.image.MemoryImageSource;

import javax.swing.JFrame;
import javax.swing.JMenuBar;

import de.wussen.gui.imagePanel.CenterPanel;
import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.utils.Log;

public class View extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private CenterPanel centerPanel;
	private SouthPanelJScrollBar southPanelJScrollBar;
	private SouthPanelImageSelection southPanelImageSelection;
	private WestPanelToolBox westPanelToolBox;
	private NorthPanelMoreTools northPanelMoreTools;
	private JMenuBar menuBar;
	private Model model;
	
	public View(Model model) {
		Log.info("Creating View <" + this.getClass() + ">");
		this.model = model;
		setTitle(Model.TITLE);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		setLayout(new BorderLayout());
		
		northPanelMoreTools = new NorthPanelMoreTools(model);
		centerPanel = new CenterPanel(model, northPanelMoreTools.getTpSave());
		southPanelImageSelection = new SouthPanelImageSelection(model, centerPanel);
		southPanelJScrollBar = new SouthPanelJScrollBar(model, southPanelImageSelection);
		westPanelToolBox = new WestPanelToolBox(model);
		
		add(BorderLayout.CENTER, centerPanel);
		add(BorderLayout.SOUTH, southPanelJScrollBar);
		add(BorderLayout.WEST, westPanelToolBox);
		add(BorderLayout.NORTH, northPanelMoreTools);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
	}
	
	public JMenuBar getViewMenuBar() {
		return this.menuBar;
	}

	public CenterPanel getCenterPanel() {
		return this.centerPanel;
	}

	public SouthPanelImageSelection getSouthPanel() {
		return this.southPanelImageSelection;
	}
	
	public NorthPanelMoreTools getNorthPanelMoreTools() {
		return northPanelMoreTools;
	}

	public Picture createWhitePicture() {
		Log.info("Creating a white Image <" + this.getClass() + ">");
		int[] arr = new int[model.getWidth() * model.getHeight()];
		for (int i = 0; i < arr.length; i++) {
			arr[i] = 255 << 24 | 0x00FFFFFF;
		}
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), arr, 0, model.getWidth());
		mis.setAnimated(true);
		return new Picture(arr, createImage(mis));
	}
	
}
