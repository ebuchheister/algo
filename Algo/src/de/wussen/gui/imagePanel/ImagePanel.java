package de.wussen.gui.imagePanel;

import java.awt.Graphics;
import java.awt.image.MemoryImageSource;

import javax.swing.JPanel;

import de.wussen.main.Matrix;
import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.utils.Log;

public class ImagePanel extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private Picture picture;
	private Picture picturePointer;
	private Picture secondLayer;
	private boolean changed;
	private int left = 0;
	private int top = 0;
	private int right = 0;
	private int bottom = 0;
	
	/**
	 * Sets the margin of the picture within the ImagePanel
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 */
	protected void setMargin(int left, int top, int right, int bottom) {
		this.left = left;
		this.top = top;
		this.right = right;
		this.bottom = bottom;
	}
	
	/**
	 * Sets the the Picture object for the ImagePanel.
	 * After setting a repaint and a revalidate is being called.
	 * @param picture
	 */
	public void setPicture(Picture picture) {
		setPicture(picture, null);
	}
	
	/**
	 * Sets the the Picture object for the ImagePanel.
	 * After setting a repaint and a revalidate is being called.
	 * @param picture
	 * @param picturePointer to the original
	 */
	public void setPicture(Picture picture, Picture picturePointer) {
		this.picture = picture;
		this.picturePointer = picturePointer;
		this.changed = false;
		repaint();
		revalidate();
	}

	/**
	 * 
	 */
	protected void savePictureToSelection() {
		this.picturePointer.setImage(picture.getImage());
		this.picturePointer.setArr(picture.getArr().clone());
		this.changed = false;
	}
	
	/**
	 * Returned the Picture object of the drawn image
	 * @return Picture
	 */
	public Picture getPicture() {
		return this.picture;
	}
	
	/**
	 * Sets the the Picture object on the second layer of the ImagePanel.
	 * After setting a repaint and a revalidate is being called.
	 * @param picture
	 */
	public void setSecondLayer(Picture picture) {
		this.secondLayer = picture;
		repaint();
		revalidate();
	}
	
	/**
	 * Returned the Picture on the secondLayer
	 * @return Picture
	 */
	public Picture getSecondLayer() {
		return this.secondLayer;
	}

	public void clearSecondLayer(Model model) {
		Log.info("Clear second layer <" + this.getClass() + ">");
		int[] rgb = this.secondLayer.getArr();
		for (int i = 0; i < rgb.length; i++) {
			rgb[i] = 0x00000000;
		}
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
		mis.setAnimated(true);
		this.secondLayer.setArr(rgb);
//		this.secondLayer.setImage(createImage(mis));
		this.secondLayer.setImage(null);
		this.secondLayer.setxOffset(0);
		this.secondLayer.setyOffset(0);
		this.secondLayer.setLastUsedMatrix( new Matrix(new double[][] {	{ 1, 0, 0}, 
																		{ 0, 1, 0},
																		{ 0, 0, 1}}));
		this.secondLayer.setChanged(false);
	}
	
	/**
	 * set changed to true.
	 * If changed is true, there will be an confirmationDialog when selecting a new Image
	 */
	public void setFlag() {
		this.changed = true;
	}
	
	/**
	 * set changed to true.
	 * If changed is true, there will be an confirmationDialog when selecting a new Image
	 */
	protected void freeFlag() {
		this.changed = false;
	}
	
	/**
	 * @return boolean -> true if the something changed on the displayed image.
	 */
	public boolean isChanged() {
		return this.changed;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		if(picture != null) {	
			g.drawImage(picture.getImage(), left, top, this.getWidth()-right-left, this.getHeight()-bottom-top, this);
		}
		if(secondLayer != null) {
			g.drawImage(secondLayer.getImage(), left, top, this.getWidth()-right-left, this.getHeight()-bottom-top, this);
		}
	}

	public Picture getPicturePointer() {
		return picturePointer;
	}

	public void setPicturePointer(Picture picturePointer) {
		this.picturePointer = picturePointer;
	}
	
}
