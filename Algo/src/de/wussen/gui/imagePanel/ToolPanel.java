package de.wussen.gui.imagePanel;

import java.awt.Color;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.utils.Log;

public class ToolPanel extends ImagePanel {
	
	private static final String PRESSED = "Pressed";
	private static final String BLOCKED = "Blocked";
	private static final String TYP = ".png";
	
	private static final long serialVersionUID = 1L;
	private Picture pictureUnselected;
	private Picture pictureSelected;
	private Picture pictureBlocked;
	private boolean selected = false;
	private boolean blockable;
	private boolean blocked;
	
	public ToolPanel(Picture pictureUnselected, Picture pictureSelected, String name) {
		this.pictureUnselected = pictureUnselected;
		this.pictureSelected = pictureSelected;
		setName(name);
		setBackground(Color.DARK_GRAY);
		setPicture(pictureUnselected);
		setPreferredSize(new Dimension(34, 34));
		setMaximumSize(new Dimension(34, 34));
		setMargin(1, 1, 1, 1);
	}
	
	public ToolPanel(String imgName) {
		init(imgName, false);
	}

	public ToolPanel(String imgName, boolean blockable) {
		init(imgName, blockable);
	}
	
	private void init(String imgName, boolean blockable) {
		Log.info("Creating tool = " + imgName + " blockable = " + blockable + " <" + this.getClass() + ">");
		this.blockable = blockable;
		this.blocked = false;
		try {
			this.pictureUnselected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + imgName + TYP)));
			this.pictureSelected = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + imgName + PRESSED + TYP)));
		} catch (IOException e) {
			Log.error("Could not load images = " + imgName + " <" + this.getClass() + ">");
		}
		if(blockable) {
			try {
				this.pictureBlocked = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + imgName + BLOCKED + TYP)));
			} catch (IOException e) {
				Log.error("Could not load a 'Blocked' images for = " + imgName + " <" + this.getClass() + ">");
				try {
					this.pictureBlocked = new Picture(null, ImageIO.read(new File("resource" + File.separatorChar + "icons" + File.separatorChar + imgName + TYP)));
				} catch (IOException e2) {
					Log.error("Could not load images = " + imgName + " <" + this.getClass() + ">");
				}
			}
		}
		setName(imgName);
		setBackground(Color.DARK_GRAY);
		if(!blockable) {
			setPicture(pictureUnselected);
		} else {
			setPicture(pictureBlocked);
		}
		setPreferredSize(new Dimension(34, 34));
		setMaximumSize(new Dimension(34, 34));
		setMargin(1, 1, 1, 1);
	}
	
	public void setSelectedPicture() {
		if(blockable ? !blocked : true) {
			if(selected) {
				setPicture(pictureUnselected);
			} else {
				setPicture(pictureSelected);
			}
			selected = !selected;
		}
	}
	
	public void unselect() {
		selected = false;
		setPicture(pictureUnselected);
	}

	public boolean isBlocked() {
		return this.blocked;
	}

	public void setBlocked(boolean blocked) {
		if(blockable) {
			this.blocked = blocked;
			if(blocked) {
				setPicture(pictureBlocked);
			} else {
				setPicture(pictureUnselected);
			}
		}
	}
	
}

