package de.wussen.gui.imagePanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import de.wussen.model.Model;
import de.wussen.model.Picture;

public class ImageSelectionPanel extends ImagePanel {

	private static final long serialVersionUID = 1L;
	private Model model;
	
	public ImageSelectionPanel(Model model) {
		this.model = model;
		setBackground(Color.GRAY);
		setPreferredSize(new Dimension(120, 120));
		setMaximumSize(new Dimension(120, 120));
		setMargin(10, 10, 10, 10);
	}

	public void setSelected(boolean selected) {
		getPicture().setSelected(selected);
		repaint();
	}
	
	public boolean isSelected() {
		return getPicture().isSelected();
	}
	
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if(getPicture().isSelected()) {
			g.drawImage(model.getSelectedImage(), 0, 0, 120, 120, this);
		} else {
			g.drawImage(model.getDeselectedImage(), 0, 0, 120, 120, this);
		}
	}
	
}
