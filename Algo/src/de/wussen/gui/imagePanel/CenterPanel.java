package de.wussen.gui.imagePanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.MemoryImageSource;
import java.time.chrono.MinguoDate;
import java.util.HashMap;
import java.util.Map.Entry;

import de.wussen.gui.ApproximationSliderPanel;
import de.wussen.main.Matrix;
import de.wussen.model.ApproximationColor;
import de.wussen.model.Cache;
import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.model.Vector;
import de.wussen.utils.AlgoUtils;
import de.wussen.utils.Log;

public class CenterPanel extends ImagePanel {
	
	private static final long serialVersionUID = 1L;
	private Model model;
	private ToolPanel tpSave;
	
	public CenterPanel(Model model, ToolPanel tpSave) {
		this.model = model;
		this.tpSave = tpSave;
		setPicture(new Picture(new int[model.getWidth() * model.getHeight()], null));
		setSecondLayer(new Picture(new int[model.getWidth() * model.getHeight()], null));
		setPreferredSize(new Dimension(model.getWidth(), model.getHeight()));
	}
	
	private void setMainChanged() {
		tpSave.setBlocked(false);	
		setFlag();
	}
	
	public void setMainNotChanged() {
		tpSave.setBlocked(true);	
		freeFlag();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		
		String selectedTool = model.getSelectedTool();
		if(selectedTool.equals("MOUSE") && !getSecondLayer().isChanged()) {
			drawMouse(g);
		} else {
			if(model.getMousePressed2() != null) {
				if (selectedTool.equals("LINE")) {
					drawLine(g, false);
				} else if (selectedTool.equals("LINECOLORED")) {
					drawLine(g, true);
				} else if (selectedTool.equals("CIRCLE")) {
					drawCircle(g, false, false);
				} else if (selectedTool.equals("CIRCLEFILLED")) {
					drawCircle(g, true, false);
				} else if (selectedTool.equals("CIRCLECOLORED")) {
					drawCircle(g, false, true);
				} else if (selectedTool.equals("CIRCLECOLOREDFILLED")) {
					drawCircle(g, true, true);
				} else if (selectedTool.equals("PENCIL")) {
					drawPoints(g);
				}
				setMainChanged();
				super.paintComponent(g);
			}
		}
	}

	private void drawPoints(Graphics g) {
		int rgb[] = getPicture().getArr().clone();
		
		setPixel(g, model.getMousePressed2().x, model.getMousePressed2().y, rgb, model.getColorActive());
		
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
		mis.setAnimated(true);
		getPicture().setArr(rgb);
		getPicture().setImage(createImage(mis));
		model.setMousePressed1(null);
		model.setMousePressed2(null);
		model.setDraw(false);
	}

	private void drawMouse(Graphics g) {
		if(model.getMousePressed1() != null) {
			
			int x1, x2, y1, y2;
			
			if(model.getMousePressed1().x <= model.getMousePressed2().x) {
				x1 = model.getMousePressed1().x;
				x2 = model.getMousePressed2().x;
			} else {
				x1 = model.getMousePressed2().x;
				x2 = model.getMousePressed1().x;
			}
			
			if(model.getMousePressed1().y <= model.getMousePressed2().y) {
				y1 = model.getMousePressed1().y;
				y2 = model.getMousePressed2().y;
			} else {
				y1 = model.getMousePressed2().y;
				y2 = model.getMousePressed1().y;
			}
			
			g.setColor(new Color(50, 150, 255, 255));
			g.drawRect(x1, y1, x2 - x1, y2 - y1);
			g.setColor(new Color(72, 223, 255, 64));
			g.fillRect(x1, y1, x2 - x1, y2 - y1);
//			System.out.println(model.getMousePressed1().x + " | " + model.getMousePressed1().y + "   -    " + model.getMousePressed2().x + " | " + model.getMousePressed2().y);
		}
	}
	
	private void drawLine(Graphics g, boolean colored) {
		int rgb[] = getPicture().getArr().clone();
		
		final double xFactor = ((double) this.getWidth()) / ((double) model.getWidth());
		final double yFactor = ((double) this.getHeight()) / ((double) model.getHeight());

		final int x0 = (int) (model.getMousePressed1().x / xFactor);
		final int y0 = (int) (model.getMousePressed1().y / yFactor);
		final int x1 = (int) (model.getMousePressed2().x / xFactor);
		final int y1 = (int) (model.getMousePressed2().y / yFactor);
		
		final int dx = Math.abs(x0 - x1);
		final int dy = Math.abs(y0 - y1);
		final int sgnDx = x0 < x1 ? 1 : -1;
		final int sgnDy = y0 < y1 ? 1 : -1;
		int shortD;
		int longD;
		int incXshort;
		int incXlong;
		int incYshort;
		int incYlong;
		
		if (dx > dy) {
			shortD = dy; 
			longD = dx; 
			incXlong = sgnDx; 
			incXshort = 0; 
			incYlong = 0; 
			incYshort = sgnDy;
		} else {
			shortD = dx; 
			longD = dy; 
			incXlong = 0; 
			incXshort = sgnDx; 
			incYlong = sgnDy; 
			incYshort = 0;
		}
		
		int d = longD / 2;
		int x = x0;
		int y = y0;
		
		for (int i = 0; i <= longD; ++i) {
			Color color;
			if(colored) {
				color = colorFade((int) (((float) i / (float)(longD-1)) * 100f));
			} else {
				color = model.getColorActive();
			}
			setPixel(g, x, y, rgb, color);
			x += incXlong;
			y += incYlong;
			d += shortD;
			if(d >= longD) {
				d -= longD;
				x += incXshort;
				y += incYshort;
			}
		}
		
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
		mis.setAnimated(true);
		if(model.isDraw()) {
			Log.info("Drawing a line (colored = " + colored + ") on ImagePanel from (" + model.getMousePressed1().x + "/" + model.getMousePressed1().y + ") to ("  + model.getMousePressed1().x + "/" + model.getMousePressed1().y + ") <" + this.getClass() + ">");
			getPicture().setArr(rgb);
			getPicture().setImage(createImage(mis));
			model.setMousePressed1(null);
			model.setMousePressed2(null);
			model.setDraw(false);
		} else {
			getPicture().setImage(createImage(mis));
		}
	}
	
	private Color colorFade(int percent) {
		Color color1 = model.getColorLeft();
		Color color2 = model.getColorRight();
		int red   = singleSchuffle(color1.getRed(), color2.getRed(), percent);
		int green = singleSchuffle(color1.getGreen(), color2.getGreen(), percent);
		int blue  = singleSchuffle(color1.getBlue(), color2.getBlue(), percent);
		return new Color(red & 255, green & 255, blue & 255, 255);
	}
	
	private int singleSchuffle(int rgb1, int rgb2, int percent) {
		return rgb1 + (rgb2 - rgb1) * percent / 100;
	}

	private void drawCircle(Graphics g, boolean filled, boolean colored) {
		int rgb[] = getPicture().getArr().clone();
		
		final int x0 = model.getMousePressed1().x;
		final int y0 = model.getMousePressed1().y;
		final int x1 = model.getMousePressed2().x;
		final int y1 = model.getMousePressed2().y;
		
		double xFactor = ((double) this.getWidth()) / ((double) model.getWidth());
		double yFactor = ((double) this.getHeight()) / ((double) model.getHeight());
		
		int[] pos = AlgoUtils.calcMousePos(new Point(0,0),  new Point(x1 - x0, y1 - y0), this, model);
		final int r = (int) Math.sqrt(Math.pow(pos[2] - pos[0], 2) + Math.pow(pos[3] - pos[1], 2));
		pos = AlgoUtils.calcMousePos(model.getMousePressed1(), model.getMousePressed2(), this, model);
		drawCircleAlgo(g, (int) (x0 / xFactor), (int) (y0 / yFactor), r, rgb, filled, colored);
		
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
		mis.setAnimated(true);
		if(model.isDraw()) {
			Log.info("Drawing a circle (filled = " + filled + ", colored = " + colored + ") <" + this.getClass() + ">");
			getPicture().setArr(rgb);
			getPicture().setImage(createImage(mis));
			model.setMousePressed1(null);
			model.setMousePressed2(null);
			model.setDraw(false);
		} else {
			getPicture().setImage(createImage(mis));
		}		
	}
	
	private void drawCircleAlgo(Graphics g, int x0, int y0, int r, int[] rgb, boolean filled, boolean colored) {
		if(colored) {
			int y = 0;
			int x = r;
			int F = -r;
			int dy = 1;
			int dyx = -2*r+3;
			while(y<=x) {
				setPixelsCircleColored(g, x0, y0, x, y, r, rgb, filled);
				++y;
				dy += 2;
				dyx += 2;
				if(F > 0) {
					F += dyx;
					--x;
					dyx +=2;
				} else {
					F += dy;
				}
			}
		} else {
			int y = 0;
			int x = r;
			int F = -r;
			int dy = 1;
			int dyx = -2*r+3;
			while(y<=x) {
				setPixelsCircle(g, x0, y0, x, y, r, rgb, filled);
				++y;
				dy += 2;
				dyx += 2;
				if(F > 0) {
					F += dyx;
					--x;
					dyx +=2;
				} else {
					F += dy;
				}
			}
		}
	}

	private void setPixelsCircle(Graphics g, int x0, int y0, int x, int y, int r, int[] rgb, boolean filled) {
		if(!filled) {
			Color color = model.getColorActive();
			setPixel(g, x0 + x, y0 +  y, rgb, color);
			setPixel(g, x0 + x, y0 -  y, rgb, color);
			setPixel(g, x0 - x, y0 +  y, rgb, color);
			setPixel(g, x0 - x, y0 -  y, rgb, color);
			setPixel(g, x0 + y, y0 +  x, rgb, color);
			setPixel(g, x0 + y, y0 -  x, rgb, color);
			setPixel(g, x0 - y, y0 +  x, rgb, color);
			setPixel(g, x0 - y, y0 -  x, rgb, color);
		} else {
			drawLineInnerCircle(g, x0 - x, y0 + y, x0 + x, y0 + y, rgb, false);
			drawLineInnerCircle(g, x0 - y, y0 + x, x0 + y, y0 + x, rgb, false);
			drawLineInnerCircle(g, x0 - x, y0 - y, x0 + x, y0 - y, rgb, false);
			drawLineInnerCircle(g, x0 - y, y0 - x, x0 + y, y0 - x, rgb, false);
		}
	}
	
	private void setPixelsCircleColored(Graphics g, int x0, int y0, int x, int y, int r, int[] rgb, boolean filled) {
		if(!filled) {
			Color color;
			color = colorFade((int) (((float) ( x + r) / (float)(r+r)) * 100f));
			setPixel(g, x0 + x, y0 +  y, rgb, color);
			setPixel(g, x0 + x, y0 -  y, rgb, color);
			color = colorFade((int) (((float) (-x + r) / (float)(r+r)) * 100f));
			setPixel(g, x0 - x, y0 +  y, rgb, color);
			setPixel(g, x0 - x, y0 -  y, rgb, color);
			color = colorFade((int) (((float) ( y + r) / (float)(r+r)) * 100f));
			setPixel(g, x0 + y, y0 +  x, rgb, color);
			setPixel(g, x0 + y, y0 -  x, rgb, color);
			color = colorFade((int) (((float) (-y + r) / (float)(r+r)) * 100f));
			setPixel(g, x0 - y, y0 +  x, rgb, color);
			setPixel(g, x0 - y, y0 -  x, rgb, color);
		} else {
			drawLineInnerCircle(g, x0 - x, y0 + y, x0 + x, y0 + y, rgb, true);
			drawLineInnerCircle(g, x0 - y, y0 + x, x0 + y, y0 + x, rgb, true);
			drawLineInnerCircle(g, x0 - x, y0 - y, x0 + x, y0 - y, rgb, true);
			drawLineInnerCircle(g, x0 - y, y0 - x, x0 + y, y0 - x, rgb, true);
		}
	}
	
	private void drawLineInnerCircle(Graphics g, int x0, int y0, int x1, int y1, int[] rgb, boolean colored) {
		
		final int dx = Math.abs(x0 - x1);
		final int dy = Math.abs(y0 - y1);
		final int sgnDx = x0 < x1 ? 1 : -1;
		final int sgnDy = y0 < y1 ? 1 : -1;
		int shortD;
		int longD;
		int incXshort;
		int incXlong;
		int incYshort;
		int incYlong;
		
		if (dx > dy) {
			shortD = dy; 
			longD = dx; 
			incXlong = sgnDx; 
			incXshort = 0; 
			incYlong = 0; 
			incYshort = sgnDy;
		} else {
			shortD = dx; 
			longD = dy; 
			incXlong = 0; 
			incXshort = sgnDx; 
			incYlong = sgnDy; 
			incYshort = 0;
		}
		
		int d = longD / 2;
		int x = x0;
		int y = y0;
		if(colored) {
			for (int i = 0; i <= longD; ++i) {
				Color color = colorFade((int) (((float) i / (float)(longD)) * 100f));
				setPixel(g, x, y, rgb, color);
				x += incXlong;
				y += incYlong;
				d += shortD;
				if(d >= longD) {
					d -= longD;
					x += incXshort;
					y += incYshort;
				}
			}
		} else {
			Color color = model.getColorActive();
			for (int i = 0; i <= longD; ++i) {
				setPixel(g, x, y, rgb, color);
				x += incXlong;
				y += incYlong;
				d += shortD;
				if(d >= longD) {
					d -= longD;
					x += incXshort;
					y += incYshort;
				}
			}
		}
	}
	
	private void setPixel(Graphics g, int x, int y, int[] rgb, Color color) {
		if(x >= 0 && x < model.getWidth() && y >= 0 && y < model.getHeight()) {
			try {
				rgb[y*model.getWidth() + x] = color.getAlpha() << 24 | color.getRed() << 16 | color.getGreen() << 8 | color.getBlue() ;
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}
	}

	public void setPictureFromMemoryImageSource(int[] arr, MemoryImageSource mis) {
		setPicture(new Picture(arr, createImage(mis)));
		
	}

	public void deleteSelectionOnFirstLayer(boolean fromBtnDelete) {
		if(fromBtnDelete ? (getSecondLayer().getImage() == null) : true) {
			Log.info("Delete selection on first Layer <" + this.getClass() + ">");
			if(isChanged()) {
				saveChangesMadeOnFirstLayer();
			}
			int rgb[] = getPicture().getArr();
			int[] pos = AlgoUtils.calcMousePos(model.getMousePressed1(), model.getMousePressed2(), this, model);
			
			int x = pos[0], y = pos[1];
			while(true) {
				try {
					rgb[y*model.getWidth() + x] = 0xFF000000;
				} catch (Exception e) {
					Log.warn("This message should not appear <" + this.getClass() + ">");
					break;
				}
				++x;
				if(x > pos[2]) {
					x = pos[0];
					++y;
					if(y > pos[3]) {
						break;
					}
				}
			}
			
			MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
			mis.setAnimated(true);
			getPicture().setArr(rgb);
			getPicture().setImage(createImage(mis));
		} else {
			System.err.println("Delete SecondLayer instead of FirstLayer");
			clearSecondLayer(model);
		}
		if(fromBtnDelete) {
			model.setMousePressed1(null);
			model.setMousePressed2(null);
		}
		setMainChanged();
		repaint();
	}
	

	public int[] copySelectionOrSecondLayer(boolean delete) {
		if (getSecondLayer().getImage() != null) {
			Log.info("Copy selection from second layer, cut out = " + delete + "<" + this.getClass() + ">");
			int[] cache = getSecondLayer().getArr().clone();
			if(delete) {
				clearSecondLayer(model);
			}
			model.setMousePressed1(null);
			model.setMousePressed2(null);
			repaint();
			return cache;
		} if(model.getMousePressed1() != null && model.getMousePressed2() != null) {
			Log.info("Copy selection from first layer, cut out = " + delete + "<" + this.getClass() + ">");
			int[] rgb = getPicture().getArr();
			int[] cache = new int[rgb.length];
			int[] pos = AlgoUtils.calcMousePos(model.getMousePressed1(), model.getMousePressed2(), this, model);
			
			int x = pos[0], y = pos[1];
			while(true) {
				cache[y*model.getWidth() + x] = rgb[y*model.getWidth() + x];
				if(delete) {
					rgb[y*model.getWidth() + x] = 0xFF000000;
				}
				++x;
				if(x > pos[2]) {
					x = pos[0];
					++y;
					if(y > pos[3]) {
						break;
					}
				}
			}
			
			if(delete) {
				MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
				mis.setAnimated(true);
				getPicture().setArr(rgb);
				getPicture().setImage(createImage(mis));
				setMainChanged();
			}
			model.setMousePressed1(null);
			model.setMousePressed2(null);
			repaint();
			return cache;
		}
		return null;
	}
	
	public void pasteFromCache(Cache cache) {
		Log.info("Pasting image from cache to second layer <" + this.getClass() + ">");
		int[] arr = cache.getArr();
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), arr, 0, model.getWidth());
		mis.setAnimated(true);
		setSecondLayer(new Picture(arr, createImage(mis)));
		model.setMousePressed1(cache.getMousePressed1());
		model.setMousePressed2(cache.getMousePressed2());
		setMainChanged();
		repaint();
	}

	public void removeOneColor(int i) {
		Log.info("Removing color <" + this.getClass() + ">");
		int rgb[] = getPicture().getArr();
		
		for (int j = 0; j < rgb.length; j++) {
			rgb[j] &= i;
		}
		
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
		mis.setAnimated(true);
		getPicture().setArr(rgb);
		getPicture().setImage(createImage(mis));
		repaint();
	}

	private void morph(String type, Matrix matrix) {
		Log.info("Get history matrix <" + this.getClass() + ">");
		int[] rgb = null;
		boolean selection = checkMousePressed();
		if(selection) {
			if(getSecondLayer().getImage() == null) {
				initSecondLayerOnFirstMorph();
			}
			rgb = getSecondLayer().getArr();
//			matrix = Matrix.mult(matrix, getSecondLayer().getLastUsedMatrix());
			matrix = Matrix.mult(getSecondLayer().getLastUsedMatrix(), matrix);
		} else {
			rgb = getPicture().getArr();
//			matrix = Matrix.mult(matrix, getPicture().getLastUsedMatrix());
			matrix = Matrix.mult(getPicture().getLastUsedMatrix(), matrix);
		}
		
		int[] newRgb = morph(matrix, rgb);
		
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), newRgb, 0, model.getWidth());
		mis.setAnimated(true);

		if(selection) {
			getSecondLayer().setLastUsedMatrix(matrix);
			getSecondLayer().setImage(createImage(mis));
		} else {
			getPicture().setLastUsedMatrix(matrix);
			getPicture().setImage(createImage(mis));
		}
		repaint(); 
	}

	private int[] morph(Matrix matrix, int[] rgb) {
		Log.info("Morph image or selection <" + this.getClass() + ">");
		int width = model.getWidth();
		int height = model.getHeight();
		int newRgb[] = new int[width * height];
		Vector vector = new Vector();
		
		for (int y = 0; y < height; y++) {
			for (int x = 0; x < width; x++) {
				vector.setV1AndV2(x, y);
				vector = Matrix.mult(vector, matrix);
//				vector = Matrix.mult(vector, matrix);
				if(vector.getV1() >= 0 && vector.getV1() < width && vector.getV2() >= 0 && vector.getV2() < height) {
					newRgb[y * width + x] = rgb[vector.getV2() * width + vector.getV1()];
				}
			}
		}
		setMainChanged(); 
		return newRgb;
	}
	
	/**
	 * returns an array[2] with middleX and middleY 
	 * @return int[] -> mid
	 */
	private int[] getMiddleOfPictureOrSelection() {
		int[] mid = new int[2];
		if(checkMousePressed() && model.getSelectedTool().equals("MOUSE")) {
			if(getPicture().isChanged()) {
				saveChangesMadeOnFirstLayer(); 
			}
			int[] pos = AlgoUtils.calcMousePos(model.getMousePressed1(), model.getMousePressed2(), this, model);
			mid[0] = ((int) ((pos[0] + (Math.abs(pos[2]-pos[0])) / 2))) + getSecondLayer().getxOffset();
			mid[1] = ((int) ((pos[1] + (Math.abs(pos[3]-pos[1])) / 2))) + getSecondLayer().getyOffset();
		} else {
			mid[0] = (model.getWidth()/2) + getPicture().getxOffset();;
			mid[1] = (model.getHeight()/2)  + getPicture().getyOffset();;
		}
		Log.info("Get middle of Selection on image (" + mid[0] + "/" + mid[1] + ") <" + this.getClass() + ">");
		return mid;
	}

	public void rotate(double degree) {
		int[] mid = getMiddleOfPictureOrSelection();
		morph("WOTRANS", Matrix.mult(Matrix.mult(Matrix.inverseTranslation(-mid[0], -mid[1]), Matrix.inverseRotation(Math.toRadians(degree))), Matrix.inverseTranslation(mid[0], mid[1])));
//		morph(Matrix.mult(Matrix.mult(Matrix.inverseTranslation(-mid[0], -mid[1]), Matrix.inverseRotation(Math.toRadians(degree))), Matrix.inverseTranslation(mid[0], mid[1])));
	}
	
	public void xShearing(double dx) {
		int[] mid = getMiddleOfPictureOrSelection();
		morph("WOTRANS", Matrix.mult(Matrix.mult(Matrix.inverseTranslation(-mid[0], -mid[1]), Matrix.inverseXShearing(dx)), Matrix.inverseTranslation(mid[0], mid[1])));
	}

	public void yShearing(double dy) {
		int[] mid = getMiddleOfPictureOrSelection();
		morph("WOTRANS", Matrix.mult(Matrix.mult(Matrix.inverseTranslation(-mid[0], -mid[1]), Matrix.inverseYShearing(dy)), Matrix.inverseTranslation(mid[0], mid[1])));
	}

	public void translation(double dx, double dy) {
		if(checkMousePressed() && model.getSelectedTool().equals("MOUSE")) {
			if(getPicture().isChanged()) {
				saveChangesMadeOnFirstLayer(); 
			}
			getSecondLayer().setxOffset((int) dx); 
			getSecondLayer().setyOffset((int) dy); 
		} else {
			getPicture().setxOffset((int) dx); 
			getPicture().setyOffset((int) dy); 
		}
		morph("ONLYTRANS", Matrix.inverseTranslation(dx, dy));
	}

	public void scale(double dx, double dy) {
		int[] mid = getMiddleOfPictureOrSelection();
		morph("WOTRANS", Matrix.mult(Matrix.mult(Matrix.inverseTranslation(-mid[0], -mid[1]), Matrix.inverseScale(dx, dy)), Matrix.inverseTranslation(mid[0], mid[1])));
	}
	
	public void scale(double dx, double dy, int x0, int y0) {
		morph("WOTRANS", Matrix.mult(Matrix.mult(Matrix.inverseTranslation(-x0, -y0), Matrix.inverseScale(dx, dy)), Matrix.inverseTranslation(x0, y0)));
	}

	public boolean checkMousePressed() {
		return model.getMousePressed2() != null;
	}

	/**
	 * Calculate the secondLayer from the selected area on the firstLAyer(Picture)
	 */
	public void initSecondLayerOnFirstMorph() {
		Log.info("Init second layer on first morph <" + this.getClass() + ">");
		
		int[] rgb = getPicture().getArr();
		int[] cache = new int[rgb.length];
		int[] pos = AlgoUtils.calcMousePos(model.getMousePressed1(), model.getMousePressed2(), this, model);
		int x = pos[0], y = pos[1];
		while(true) {
			try {
				cache[y * model.getWidth() + x] = rgb[y * model.getWidth() + x];
			} catch (Exception e) {
				Log.error("This message sould not appear <" + this.getClass() + ">");
				break;
			}
			++x;
			if(x > pos[2]) {
				x = pos[0];
				++y;
				if(y > pos[3]) {
					break;
				}
			}
		}

		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), cache, 0, model.getWidth());
		mis.setAnimated(true);
		setSecondLayer(new Picture(cache, createImage(mis)));
		deleteSelectionOnFirstLayer(false);
		repaint();
	}

	/**
	 * Clearing all SeconLayer data
	 */
	public void clearSecondPicture() {
		Log.info("Clear second Picture/second Layer<" + this.getClass() + ">");
		clearSecondLayer(model);
	}

	/**
	 * fusion between ImagePanel.picture and ImagePanel.secondLayer
	 */
	public void potarafusion() {
		Log.info("Potarafusion: write second layer on first layer <" + this.getClass() + ">");
		if(getSecondLayer() != null) {
			int[] rgb = getPicture().getArr();
			int[] cache = morph(getSecondLayer().getLastUsedMatrix(), getSecondLayer().getArr()); 
			
			for (int pos = 0; pos < cache.length; pos++) {
				if(((cache[pos] >> 24) & 255) != 0) {
					rgb[pos] = cache[pos];
				}
			}
			MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), rgb, 0, model.getWidth());
			mis.setAnimated(true);
			getPicture().setArr(rgb);
			getPicture().setImage(createImage(mis));
			clearSecondLayer(model);
			setMainChanged();
			repaint();
		}
	}
	
	public void saveToSelection() {
		Log.info("Save Image to south selection <" + this.getClass() + ">");
		tpSave.setBlocked(true);
		saveChangesMadeOnFirstLayer();
		if(getSecondLayer().getImage() != null) {
			potarafusion();
		}
		savePictureToSelection();
		if(model.getMousePressed1() != null || model.getMousePressed2() != null) {
			model.setMousePressed1(null);
			model.setMousePressed2(null);
		}
		setMainNotChanged();
	}

	public void saveChangesMadeOnFirstLayer() {
		Log.info("Save changes made on first layer <" + this.getClass() + ">");
		Matrix matrix = getPicture().getLastUsedMatrix();
		getPicture().setArr(morph(matrix, getPicture().getArr()));
		getPicture().setLastUsedMatrix(Matrix.getStandartMatrix());
		getPicture().setChanged(false);
	}
	
	private int[] hexKeys;
	private int[] hexQuant;

	public void approximatePreWork() {
		Log.info("Start approximate <" + this.getClass() + ">");
		Log.info("Start loading <" + this.getClass() + ">");
		
		int rgb[] = getPicture().getArr();
		int picSize = model.getWidth() * model.getHeight();
		HashMap<Integer, Integer> hm = new HashMap<>(picSize);
		
		for (int i = 0; i < rgb.length; i++) {
			int color = rgb[i];
			Integer colorCount = hm.get(color);
			if(colorCount == null) {
				colorCount = 0;
			}
			++colorCount;
			hm.put(color, colorCount);
		}
		
		hexKeys = new int[hm.size()];
		hexQuant = new int[hm.size()];
		
		int count = 0;
		for (Entry<Integer, Integer> map : hm.entrySet()) {
			if (map.getKey() != null) {
				hexKeys[count] = map.getKey();
				hexQuant[count++] = map.getValue();
			}
		}
		Log.info("End loading <" + this.getClass() + ">");
		AlgoUtils.quicksortTwoArrays(hexKeys, hexQuant); 
		
		ApproximationSliderPanel asp = new ApproximationSliderPanel(this, hexKeys.length);
	}
	
	public void approximate(double percent) {
		int[] arr = getPicture().getArr().clone();
		int newLength = (int) (hexKeys.length * percent);
		HashMap<Integer, Integer> hmNewColors = new HashMap<>((int) (hexKeys.length)); // Warning
		
		Log.info("approximate percent = " + percent + " (i=0; i<" + (hexKeys.length-newLength) + ") (i=" + (hexKeys.length-newLength) + "; i<" + (hexKeys.length) + ") <" + this.getClass() + ">");
		
		int[] arrR = new int[newLength];
		int[] arrG = new int[newLength];
		int[] arrB = new int[newLength];
		int index = 0;
		for (int i = hexKeys.length-newLength; i < hexKeys.length; i++) {
			arrR[index] = hexKeys[i];
			arrG[index] = hexKeys[i];
			arrB[index++] = hexKeys[i];
		}
		
		AlgoUtils.quicksort(arrR, 16);
		AlgoUtils.quicksort(arrG,  8);
		AlgoUtils.quicksort(arrB,  0);
		
		
		for (int i = 0; i < hexKeys.length-newLength; i++) {
			int key = hexKeys[i];
			int indexR = AlgoUtils.binarySearchNew(arrR, 16, (key >> 16) & 255); // Bullshit distance
			int indexG = AlgoUtils.binarySearchNew(arrG,  8, (key >>  8) & 255);
			int indexB = AlgoUtils.binarySearchNew(arrB,  0, (key >>  0) & 255);
			
			ApproximationColor ac = new ApproximationColor(Integer.MAX_VALUE, 0, 0);
			AlgoUtils.getclosestColor(arrR, arrG, arrB, indexR, indexG, indexB, key, ac);
			hmNewColors.put(key, ac.getNewColor());
		}
		
		for (int i = hexKeys.length-newLength; i < hexKeys.length; i++) {
			hmNewColors.put(hexKeys[i], hexKeys[i]); // second value needs to be the new one
		}
		
		for (int i = 0; i < arr.length; i++) {
			arr[i] = hmNewColors.get(arr[i]);
		}
		
		MemoryImageSource mis = new MemoryImageSource(model.getWidth(), model.getHeight(), arr, 0, model.getWidth());
		mis.setAnimated(true);
		getPicture().setImage(createImage(mis));
		repaint();
	}
	
}
