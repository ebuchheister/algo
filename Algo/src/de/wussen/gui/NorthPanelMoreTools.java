package de.wussen.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.tools.Tool;

import de.wussen.gui.imagePanel.ToolPanel;
import de.wussen.model.Model;
import de.wussen.utils.Log;

public class NorthPanelMoreTools extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private JButton btnColorLeft;
	private JButton btnColorRight;

	private ToolPanel tpAdd;
	private ArrayList<ToolPanel> toolsCopyAndPaste;
	private ToolPanel tpSave;
	private ArrayList<ToolPanel> toolsMorph;
	
	private Model model;
	
	public NorthPanelMoreTools(Model model) {
		Log.info("Creating north tools <" + this.getClass() + ">");
		this.model = model;
		setBackground(Color.GRAY);
		setLayout(new FlowLayout(FlowLayout.LEFT));
		JLabel lblBreaker0 = new JLabel(" | ");
		JLabel lblBreaker1 = new JLabel(" | ");
		JLabel lblBreaker2 = new JLabel(" | ");
		JLabel lblBreaker3 = new JLabel(" | ");
		lblBreaker0.setBackground(Color.DARK_GRAY);
		lblBreaker1.setBackground(Color.DARK_GRAY);
		lblBreaker2.setBackground(Color.DARK_GRAY);
		lblBreaker3.setBackground(Color.DARK_GRAY);
		
		btnColorLeft = new JButton();
		btnColorLeft.setPreferredSize(new Dimension(34, 34));
		btnColorLeft.setBackground(new Color(0, 0, 0));
		btnColorLeft.addActionListener(e -> changeColor(btnColorLeft, true));
		model.setColorLeft(new Color(0, 0, 0));
		
		btnColorRight = new JButton();
		btnColorRight.setPreferredSize(new Dimension(34, 34));
		btnColorRight.setBackground(new Color(255, 255, 255));
		btnColorRight.addActionListener(e -> changeColor(btnColorRight, false));
		model.setColorRight(new Color(255, 255, 255));
		
		toolsMorph = new ArrayList<>();
		toolsMorph.add(new ToolPanel("Left", true));
		toolsMorph.add(new ToolPanel("Right", true));
		toolsMorph.add(new ToolPanel("Up", true));
		toolsMorph.add(new ToolPanel("Down", true));
		toolsMorph.add(new ToolPanel("ScalePlus", true));
		toolsMorph.add(new ToolPanel("ScaleMinus", true));
		toolsMorph.add(new ToolPanel("RotateLeft", true));
		toolsMorph.add(new ToolPanel("RotateRight", true));
		toolsMorph.add(new ToolPanel("XShearingPlus", true));
		toolsMorph.add(new ToolPanel("XShearingMinus", true));
		toolsMorph.add(new ToolPanel("YShearingPlus", true));
		toolsMorph.add(new ToolPanel("YShearingMinus", true));
		
		toolsCopyAndPaste = new ArrayList<>();
		toolsCopyAndPaste.add(new ToolPanel("Copy", true));
		toolsCopyAndPaste.add(new ToolPanel("Cut", true));
		toolsCopyAndPaste.add(new ToolPanel("Paste", true));
		toolsCopyAndPaste.add(new ToolPanel("Delete", true));
		
		tpAdd = new ToolPanel("PicturePlus");
		add(tpAdd);
		add(lblBreaker0);
		
		for(ToolPanel tp : toolsCopyAndPaste) {
			add(tp);
		}
		
		add(lblBreaker1);
		tpSave = new ToolPanel("Save", true);
		tpSave.setBlocked(true);
		add(tpSave);
		add(lblBreaker2);
		
		for(ToolPanel tp : toolsMorph) {
			add(tp);
		}

		add(lblBreaker3);
		add(btnColorLeft);
		add(btnColorRight);
		
	}

	private void changeColor(JButton btn, boolean left) {
		Color newColor = JColorChooser.showDialog(this, "Choose color", btn.getBackground());
		Log.info("New color selected left = " + left + " color = " + newColor + " <" + this.getClass() + ">");
		if(newColor.getAlpha() != 0) {
			btn.setBackground(newColor);
			if(left) {
				model.setColorLeft(btn.getBackground());
			} else {
				model.setColorRight(btn.getBackground());
			}
		}
	}

	public ArrayList<ToolPanel> getToolsMorph() {
		return toolsMorph;
	}
	
	public ArrayList<ToolPanel> getToolsCopyAndPaste() {
		return toolsCopyAndPaste;
	}

	public ToolPanel getTpSave() {
		return tpSave;
	}

	public ToolPanel getTpAdd() {
		return tpAdd;
	}
	
}
