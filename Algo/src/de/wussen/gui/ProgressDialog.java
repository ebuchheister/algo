package de.wussen.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JDialog;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import de.wussen.utils.Log;

public class ProgressDialog extends JDialog {
	
	private static final long serialVersionUID = 1L;
	
	private JTextArea ta;
	private JProgressBar pb;

	public ProgressDialog(int length) {
		Log.info("Create ProgressBarDialog <" + this.getClass() + ">");
		setTitle("Loading Pictures, pleas wait...");
		setLayout(new BorderLayout());
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		ta = new JTextArea();
		JScrollPane sp = new JScrollPane(ta);
		sp.setAutoscrolls(true);
		ta.setLineWrap(true);
		ta.setWrapStyleWord(true);
		ta.setFont(new Font("COURIER", 0, 14));
		ta.setBackground(Color.BLACK);
		ta.setForeground(Color.GREEN);
		ta.setEditable(false);
		
		DefaultCaret caret = (DefaultCaret) ta.getCaret();
		caret.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

		pb = new JProgressBar(0, length);
		pb.setStringPainted(true);
		
		add(BorderLayout.SOUTH, pb);
		add(BorderLayout.CENTER, sp);

		setSize(new Dimension(600, 400));
		setLocationRelativeTo(null);
		setVisible(true);
	}

	public void progress(String absolutePath) {
		pb.setValue(pb.getValue()+1);
		ta.append("Added: " + absolutePath + "\n");
	}

}
