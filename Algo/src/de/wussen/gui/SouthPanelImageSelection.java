package de.wussen.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import de.wussen.gui.imagePanel.CenterPanel;
import de.wussen.gui.imagePanel.ImageSelectionPanel;
import de.wussen.model.Model;
import de.wussen.model.Picture;
import de.wussen.utils.Log;

public class SouthPanelImageSelection extends JPanel {

	private static final long serialVersionUID = 1L;
	private Model model;
	private CenterPanel centerPanel;
	
	public SouthPanelImageSelection(Model model, CenterPanel centerPanel) {
		this.model = model;
		this.centerPanel = centerPanel;
		setBackground(Color.GRAY);
		setLayout(new FlowLayout(FlowLayout.LEFT));
	}
	
	public void initImages() {
		Log.info("Init selection images <" + this.getClass() + ">");
		for (int i = 0; i < model.getPictures().size(); i++) {
			addImage(i);
		}
	}
	
	public void addImage(int key) {
		Log.info("Add image to selection panel <" + this.getClass() + ">");
		ImageSelectionPanel isp = new ImageSelectionPanel(model);
		isp.setPicture(model.getPictures().get(key));
		if(key == 0) {
			centerPanel.setPicture(new Picture(isp.getPicture().getArr().clone(), isp.getPicture().getImage()), isp.getPicture());
			centerPanel.getPicture().setFileName(isp.getPicture().getFileName());
		}
		isp.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				if(SwingUtilities.isRightMouseButton(e)) {
					Log.info("Select image with image = " + isp.getPicture().getFileName() + " <" + this.getClass() + ">");
					isp.setSelected(!isp.isSelected());
				} else if(SwingUtilities.isLeftMouseButton(e)) {
					if(centerPanel.isChanged()) {
						int saving = JOptionPane.showConfirmDialog(centerPanel, "Do you want to save the changes to the selection?", "Saving", JOptionPane.OK_OPTION);
						if(saving == 0) { // 0 -> ja & 1 -> nein
							centerPanel.saveToSelection();
							repaint();
						}
					}
					if(centerPanel.getSecondLayer().getImage() != null) {
						centerPanel.clearSecondLayer(model);
					}
					
					centerPanel.setPicture(new Picture(isp.getPicture().getArr().clone(), isp.getPicture().getImage()), isp.getPicture());
					centerPanel.getPicture().setFileName(isp.getPicture().getFileName());
					model.setMousePressed1(null);
					model.setMousePressed2(null);
					centerPanel.setMainNotChanged();
					centerPanel.repaint();
				}
			}
		});
		add(isp);
	}
	
}
