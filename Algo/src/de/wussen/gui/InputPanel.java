package de.wussen.gui;

import java.awt.Component;
import java.awt.Panel;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class InputPanel extends Panel {
	
	private static final long serialVersionUID = 1L;
	private Component parentComponent;
	private JTextField txtXField = new JTextField(4);
	private JTextField txtYField = new JTextField(4);
	
	private void init(Component parentComponent) {
		this.parentComponent = parentComponent;
		add(new JLabel("X: "));
		add(txtXField);
		add(Box.createHorizontalStrut(15));
		add(new JLabel("Y:"));
		add(txtYField);
	}

	public InputPanel(Component parentComponent) {
		init(parentComponent);
	}

	public InputPanel(Component parentComponent, double x, double y) {
		init(parentComponent);
		txtXField.setText(String.valueOf(x));
		txtYField.setText(String.valueOf(y));
	}

	public double getXValue() {
		try {
			return Double.parseDouble(txtXField.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(parentComponent, "The X or Y input was not a number");
		}
		return ~0;
	}
	
	public double getYValue() {
		try {
			return Double.parseDouble(txtYField.getText());
		} catch (Exception e) {
			JOptionPane.showMessageDialog(parentComponent, "The X or Y input was not a number");
		}
		return ~0;
	}
	
	

}
