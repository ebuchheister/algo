package de.wussen.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import de.wussen.gui.View;
import de.wussen.model.Model;

public class InputListener implements KeyListener {

	private Model model;
	private View view;
	
	public InputListener(Model model, View view) {
		this.model = model;
		this.view = view;
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
			model.setCtrlDown(true);
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		if(model.getSelectedTool().equals("MOUSE") && model.getMousePressed1() != null && model.getMousePressed2() != null) {
			if(e.getKeyCode() == KeyEvent.VK_DELETE) {
				view.getCenterPanel().deleteSelectionOnFirstLayer(true);
			}
//			if(e.getKeyCode() == KeyEvent.VK_C && model.isCtrlDown()) {
//				int[] chache = view.getCenterPanel().copySelection(false);
//			}
//			if(e.getKeyCode() == KeyEvent.VK_X && model.isCtrlDown()) {
//				int[] chache = view.getCenterPanel().copySelection(true);
//			}
		}
		if(e.getKeyCode() == KeyEvent.VK_CONTROL) {
			model.setCtrlDown(false);
		}
	}
	
	@Override
	public void keyTyped(KeyEvent e) {
	}

}
