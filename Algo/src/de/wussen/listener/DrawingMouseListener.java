package de.wussen.listener;

import java.awt.Point;
import java.awt.event.MouseEvent;

import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import de.wussen.gui.View;
import de.wussen.model.Model;

public class DrawingMouseListener extends MouseInputAdapter {
	
	private Model model;
	private View view;
	private int currX;
	private int currY;
	
	public DrawingMouseListener(Model model, View view) {
		this.model = model;
		this.view = view;
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		if(model.getSelectedTool().equals("MOUSE") || 
				model.getSelectedTool().equals("LINE") || 
				model.getSelectedTool().equals("LINECOLORED") || 
				model.getSelectedTool().equals("CIRCLE") || 
				model.getSelectedTool().equals("CIRCLEFILLED") || 
				model.getSelectedTool().equals("CIRCLECOLORED") || 
				model.getSelectedTool().equals("CIRCLECOLOREDFILLED") || 
				model.getSelectedTool().equals("PENCIL")) {	
			if(model.getMousePressed1() == null) {
				model.setMousePressed1(new Point(e.getX(), e.getY()));
			}
			model.setMousePressed2(new Point(e.getX(), e.getY()));
			view.getCenterPanel().repaint();
		}
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		if(SwingUtilities.isLeftMouseButton(e)) {
			model.setColorActive(model.getColorLeft());
		} else if(SwingUtilities.isRightMouseButton(e)) {
			model.setColorActive(model.getColorRight());
		}
		if(model.getSelectedTool().equals("MOUSE")) {	
			if(view.getCenterPanel().getSecondLayer().getImage() != null) {
				System.out.println(model.getMousePressed1() + " ||| " + model.getMousePressed2());
				view.getCenterPanel().potarafusion();
			}
			model.setMousePressed1(null);
			model.setMousePressed2(null);
			view.getCenterPanel().repaint();
		}
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
		if(model.getSelectedTool().equals("LINE") || 
				model.getSelectedTool().equals("LINECOLORED") || 
				model.getSelectedTool().equals("CIRCLECOLORED") || 
				model.getSelectedTool().equals("CIRCLECOLOREDFILLED") || 
				model.getSelectedTool().equals("CIRCLE") || 
				model.getSelectedTool().equals("CIRCLEFILLED") || 
				model.getSelectedTool().equals("PENCIL")) {
			model.setDraw(true);
			view.getCenterPanel().repaint();
		}
		if(model.getSelectedTool().equals("MOUSE") && model.getMousePressed2() != null) {
//			view.getCenterPanel().setSecondPicture();
//			view.getCenterPanel().deleteSelected();
		}
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		if(model.isCtrlDown()) {
			System.out.println(currX + "/" + currY);
		}
		this.currX = e.getX();
		this.currY = e.getY();
	}
	
	public int getCurrentXPos() {
		return this.currX;
	}
	
	public int getCurrentYPos() {
		return this.currY;
	}

}
