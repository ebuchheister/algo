package de.wussen.model;

public class ApproximationColor {
	
	private int minDistance;
	private int newColor;
	private int index;
	
	public ApproximationColor(int minDistance, int newColor, int index) {
		this.minDistance = minDistance;
		this.newColor = newColor;
		this.index = index;
	}
	
	public int getMinDistance() {
		return minDistance;
	}
	
	public void setMinDistance(int minDistance) {
		this.minDistance = minDistance;
	}
	
	public int getNewColor() {
		return newColor;
	}
	
	public void setNewColor(int newColor) {
		this.newColor = newColor;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setIndex(int index) {
		this.index = index;
	}

}
