package de.wussen.model;

import de.wussen.main.Matrix;

public class HistoMatrix {
	
	private Matrix woTrans = Matrix.getStandartMatrix();
	private Matrix onlyTrans = Matrix.getStandartMatrix();
	
	public HistoMatrix() {
		woTrans	  = Matrix.getStandartMatrix();
		onlyTrans = Matrix.getStandartMatrix();
	}
	
	public void setMatrix(String type, Matrix matrix) {
		if(type.equals("WOTRANS")) {
			this.woTrans = Matrix.mult(woTrans, matrix);
		}
		if(type.equals("ONLYTRANS")) {
			this.onlyTrans = Matrix.mult(onlyTrans, matrix);
		}
	}
	
	public Matrix getWoTrans() {
		return woTrans;
	}
	public Matrix getOnlyTrans() {
		return onlyTrans;
	}

	public Matrix getMatrix(Matrix matrix) {
		Matrix histoMatrix = Matrix.mult(matrix, woTrans);
		histoMatrix = Matrix.mult(onlyTrans, histoMatrix);
		return histoMatrix;
	}
	
//	public Matrix getHistoMatrix(Matrix matrix) {
//		Matrix histoMatrix;
//		histoMatrix = Matrix.mult(, getRotate());
//		histoMatrix = Matrix.mult(histoMatrix, getxShearing());
//		histoMatrix = Matrix.mult(histoMatrix, getyShearing());
//		histoMatrix = Matrix.mult(histoMatrix, getScale());
//		histoMatrix = Matrix.mult(histoMatrix, getTranslation());
//		return histoMatrix;
//	}

}
