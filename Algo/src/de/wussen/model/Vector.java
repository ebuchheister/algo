package de.wussen.model;


public class Vector {
	
	private int v1;
	private int v2;
	private final int v3 = 1;
	
	public Vector() {
	}
	
	public Vector(int v1, int v2) {
		this.v1 = v1;
		this.v2 = v2;
	}
	
	public void setV1AndV2(int v1, int v2) {
		this.v1 = v1;
		this.v2 = v2;
	}

	public int getV1() {
		return v1;
	}
	
	public void setV1(int v1) {
		this.v1 = v1;
	}
	
	public int getV2() {
		return v2;
	}
	
	public void setV2(int v2) {
		this.v2 = v2;
	}
	
	public int getV3() {
		return v3;
	}
	
}