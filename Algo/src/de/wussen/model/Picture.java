package de.wussen.model;

import java.awt.Image;

import de.wussen.main.Matrix;

public class Picture {
	
	private int[] arr;
	private Image img;
	private Matrix lastUsedMatrix;
	private boolean selected;
	private boolean changed;
	private int xOffset;
	private int yOffset;
	private String fileName;
	
	public Picture(int[] arr, Image img) {
		this.arr = arr;
		this.img = img;
		this.lastUsedMatrix = Matrix.getStandartMatrix();
		this.selected = false;
		this.changed = false;
		this.xOffset = 0;
		this.yOffset = 0;
	}

	public Picture(int[] arr, Image img, String name) {
		this.arr = arr;
		this.img = img;
		this.lastUsedMatrix = Matrix.getStandartMatrix();
		this.selected = false;
		this.changed = false;
		this.xOffset = 0;
		this.yOffset = 0;
		this.fileName = name;
	}

	public int[] getArr() {
		return arr;
	}

	public Image getImage() {
		return this.img;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public void setImage(Image img) {
		this.img = img;
	}

	public void setArr(int[] arr) {
		this.arr = arr;
	}

	public Matrix getLastUsedMatrix() {
		return lastUsedMatrix;
	}

	public void setLastUsedMatrix(Matrix matrix) {
		this.lastUsedMatrix = matrix;
		this.changed = true;
	}

	public boolean isChanged() {
		return changed;
	}

	public void setChanged(boolean changed) {
		this.changed = changed;
	}

	public void saveChanges() {
		
	}

	public int getxOffset() {
		return xOffset;
	}

	public void setxOffset(int xOffset) {
		this.xOffset += xOffset;
	}

	public int getyOffset() {
		return yOffset;
	}

	public void setyOffset(int yOffset) {
		this.yOffset += yOffset;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
