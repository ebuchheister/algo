package de.wussen.model;

import java.awt.Point;

public class Cache {

	private int[] arr;
	private Point mousePressed1;
	private Point mousePressed2;
	
	public Cache(int[] arr, Point mousePressed1, Point mousePressed2) {
		this.arr = arr.clone();
		this.mousePressed1 = mousePressed1;
		this.mousePressed2 = mousePressed2;
	}
	
	public int[] getArr() {
		return arr.clone();
	}

	public Point getMousePressed1() {
		return mousePressed1;
	}

	public Point getMousePressed2() {
		return mousePressed2;
	}

}

