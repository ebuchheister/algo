package de.wussen.model;

import java.awt.Color;
import java.awt.Image;
import java.awt.Point;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.imageio.ImageIO;

import de.wussen.gui.imagePanel.ToolPanel;
import de.wussen.utils.Log;

public class Model {

	public final static String TITLE = "Algo 3.1"; 
	
	private int width;
	private int height;
	
	private Vector<Picture> pictures;
	private Vector<Picture> selectedPictures;
	
	private Cache cache;
	
	private Point MousePressed1;
	private Point MousePressed2;
	private boolean draw;
	
	private Image selectedImage;
	private Image deselectedImage;
	
	private ArrayList<ToolPanel> tools;
	private String selectedTool;

	private Color colorLeft;
	private Color colorRight;
	private Color colorActive;
	
	private boolean savingBlocked;
	private boolean ctrlDown;
	private boolean dev;
	private boolean full;
	
	public Model(int width, int height, boolean dev, boolean full) {
		Log.info("Creating Model with = " + width + ", height = " + height + " <" + this.getClass() + ">");
		this.width = width;
		this.height = height;
		this.selectedTool = "";
		this.pictures = new Vector<>();
		this.selectedPictures = new Vector<>();
		this.draw = false;
		this.dev = dev;
		this.full = full;
//		this.chache = new int[width * height];
		try {
			selectedImage = ImageIO.read(new File("resource" + File.separatorChar + "SelectedTrans.png"));
			deselectedImage = ImageIO.read(new File("resource" + File.separatorChar + "DeselectedTrans.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}
	
	public int addPicture(int[] arr, Image img, String name) {
		this.pictures.add(new Picture(arr, img, name));
		return pictures.size()-1;
	}
	

	public int addPicture(Picture picture) {
		picture.setFileName("Blank_" + pictures.size());
		this.pictures.add(picture);
		return pictures.size()-1;
	}
	
	public Vector<Picture> getPictures() {
		return this.pictures;
	}
	
	public Vector<Picture> getSelectedPictures() {
		return selectedPictures;
	}
	
	public void setSelectedPictures(Vector<Picture> selectedPictures) {
		this.selectedPictures = selectedPictures;
	}

	public void addPictureToSelectPictureVector(Picture picture) {
		this.selectedPictures.add(picture);
	}

	public Point getMousePressed1() {
		return MousePressed1;
	}

	public void setMousePressed1(Point mousePressed1) {
		MousePressed1 = mousePressed1;
	}

	public Point getMousePressed2() {
		return MousePressed2;
	}

	public void setMousePressed2(Point mousePressed2) {
		MousePressed2 = mousePressed2;
	}
	
	public Image getSelectedImage() {
		return selectedImage;
	}

	public Image getDeselectedImage() {
		return deselectedImage;
	}

	public ArrayList<ToolPanel> getTools() {
		return tools;
	}

	public void setTools(ArrayList<ToolPanel> tools) {
		this.tools = tools;
	}

	public void setSelectedTool(String selectedTool) {
		this.selectedTool = selectedTool;
		if(!(selectedTool.equals("MOUSE"))) {
			setMousePressed1(null);
			setMousePressed2(null);
		}
	}

	public String getSelectedTool() {
		return this.selectedTool;
	}

	public boolean isDraw() {
		return draw;
	}

	public void setDraw(boolean draw) {
		this.draw = draw;
	}

	public Color getColorLeft() {
		return colorLeft;
	}

	public void setColorLeft(Color colorLeft) {
		this.colorLeft = colorLeft;
	}

	public Color getColorRight() {
		return colorRight;
	}

	public void setColorRight(Color colorRight) {
		this.colorRight = colorRight;
	}

	public Color getColorActive() {
		return colorActive;
	}

	public void setColorActive(Color colorActive) {
		this.colorActive = colorActive;
	}
	
	public void setCtrlDown(boolean ctrlDown) {
		this.ctrlDown = ctrlDown;
	}

	public boolean isCtrlDown() {
		return this.ctrlDown;
	}

	public boolean isDev() {
		return dev;
	}

	public boolean isFull() {
		return full;
	}

	public Cache getCache() {
		return this.cache;
	}

	public void setCache(int[] cache, Point mousePressed1, Point mousePressed2) {
		this.cache = new Cache(cache, mousePressed1, mousePressed2);
	}

	public boolean isSavingBlocked() {
		return this.savingBlocked;
	}
	
	public void setSavingBlocked(boolean savingBlocked) {
		this.savingBlocked = savingBlocked;
	}
	
}
